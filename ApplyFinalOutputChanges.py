import sys
import time
import requests
from collections import defaultdict

#This file:
#Adds Dollar Values to each transaction
#Replaces Wallet Addresses with Recognized Addresses

def fetchCoinPrice(coinSymbol,epochtime=int(time.time())):
	apiURL = "https://min-api.cryptocompare.com/data/pricehistorical?fsym={}&tsyms=USD&ts={}".format(coinSymbol,epochtime)
	r = requests.get(apiURL)
	print(r.json())
	coinPrice = r.json().get(coinSymbol)
	if coinPrice and coinPrice["USD"]!=0:
		return coinPrice["USD"]
	return "Not Found"

def loadAddressBook():
	addressBook = {}
	addressBookFile = open("UserInputFiles/AddressBook.csv").read().split("\n")
	for line in addressBookFile:
		if line:
			address,label = line.split(",")
			address = address.lower()
			addressBook[address] = label
	return addressBook

def loadTxBook():
	txBook = {}
	txBookFile = open("UserInputFiles/TransactionBook.csv").read().split("\n")
	for line in txBookFile:
		txID,label = line.split(",")
		txID = txID.lower()
		txBook[txID] = label
	return txBook	

def loadUserICOBook():
	icoBook = defaultdict(list)
	icoDataFile = open("UserInputFiles/{}/ManualTransactions/{}ICOList.csv".format(user,user),"r").readlines()
	for line in icoDataFile[1:]:
		keys = "Payment_Date,Distribution_Date,Sell_Quantity,Sell_Coin,Buy_Quantity,ICO_Coin".split(",")
		splitLine = line.strip().split(",")
		ICO_Coin = splitLine[-1]
		Sell_Coin = splitLine[-3]
		icoBook[ICO_Coin].append({key:val for key,val in zip(keys,splitLine)})
		icoBook[Sell_Coin].append({key:val for key,val in zip(keys,splitLine)})
	return icoBook

def loadTradeBook():
	tradeBook = {}
	tradeData = open("{}TradeBook.csv".format(user),"r").readlines()
	for line in tradeData:
		splitLine = line.strip().split(",")
		date = splitLine[-2]
		keys = "Action,Buy_Quantity,Buy_Coin,Sell_Quantity,Sell_Coin,Exchange,To_Address,From_Address,TxID,Buy_Coin_Price,Sell_Coin_Price,Date,Taxable".split(",")
		lineWithoutTaxableColumn = ",".join(splitLine[:-1])
		tradeBook[lineWithoutTaxableColumn] = splitLine[-1]
	return tradeBook

def checkForMissingPriceHistory(isWithdrawal,isDeposit,Buy_Coin,buyCoinUSDValue,Sell_Coin,sellCoinUSDValue,line):
	if not isWithdrawal and buyCoinUSDValue in ["0","Not Found"]:
		#print("PRICE HISTORY ERROR: {},{},{}".format(Buy_Coin,getEpochtime(",".join(line)),buyCoinUSDValue))
		return True
	if not isDeposit and sellCoinUSDValue in ["0","Not Found"]:
		#print("PRICE HISTORY ERROR: {},{},{}".format(Sell_Coin,getEpochtime(",".join(line)),sellCoinUSDValue))
		return True
	return False

def addTaxableColumn(seenCoins,dailyTxs,txBook,icoBook,listWithoutTaxableColumn):
	Action,Buy_Quantity,Buy_Coin,Sell_Quantity,Sell_Coin,Exchange,ToAddress,FromAddress,TxID,buyCoinUSDValue,sellCoinUSDValue,date = listWithoutTaxableColumn
	taxable = ""
	# dayEpochtime = time.mktime(time.strptime(date.split(" ")[0],"%Y-%m-%d"))
	# print(listWithoutTaxableColumn)
	#Conditions
	keywords = ["exchange","bittrex","binance","ledger","paper","etherdelta","gemini","eidoo","desktop","coinbase"]
	toRecognizedAddress = any(map(lambda x:x in ToAddress.lower(),keywords))
	fromRecognizedAddress = any(map(lambda x:x in FromAddress.lower(),keywords))
	toAndFromRecognizedAddress = toRecognizedAddress and fromRecognizedAddress
	toPersonalWallet = any(map(lambda x:x in ToAddress.lower(),["ledger","paper","eidoo","desktop"]))
	fromPersonalWallet = any(map(lambda x:x in FromAddress.lower(),["ledger","paper","eidoo","desktop"]))
	isDeposit = Action=="Deposit"
	isWithdrawal = Action=="Withdrawal"
	isDepositFromPersonalWallet = isDeposit and fromPersonalWallet
	isWithdrawalToPersonalWallet = isWithdrawal and toPersonalWallet
	isCoinbaseDeposit = ("coinbase" in Exchange.lower()) and isDeposit	
	minedDASH = isDeposit and Buy_Coin == "DASH" and 0.098 <= float(Buy_Quantity) <= 0.12
	minedZEC = isDeposit and Buy_Coin == "ZEC" and 0.0 <= float(Buy_Quantity) <= 0.03
	minedLTC = isDeposit and Buy_Coin == "LTC" and 0.0 <= float(Buy_Quantity) in [0.2,0.5]
	minedBTC = isDeposit and Buy_Coin == "BTC" and 0.0 <= float(Buy_Quantity) <= 0.13
	minedETH = isDeposit and Buy_Coin == "ETH" and ((0.9 <= float(Buy_Quantity) <= 1.1) or (0.09 <= float(Buy_Quantity) <= 0.1))
	notBigCoin = not Buy_Coin in ["ETH","BTC","LTC","ZEC","DASH"]
	fromPool = any(map(lambda x:x in FromAddress.lower(),["pool","ethermine"]))
	isFork = "fork" in FromAddress.lower()
	toMiningAddress = any(map(lambda x:x in ToAddress.lower(),["mining","mine"]))
	toCrowdsale = "crowdsale" in ToAddress.lower()
	fromCrowdsale = "crowdsale" in FromAddress.lower()
	isTrade = Action=="Trade"
	isPayment = "payment" in ToAddress.lower()
	isAirDropCoin = Buy_Coin in ["GOD","ETF", "ABTC","LRC","BCD","BTG","UKG","KCS","XNN","GAS","JOT","VIU","TRX"]
	boughtWithUSD = isTrade and "USD" == Sell_Coin
	usdDeposit = (not Sell_Coin) and (Buy_Coin=="USD")
	usdWithdrawal = (not Buy_Coin) and (Sell_Coin=="USD")
	isDepositCoinAndQuantityWithin5Percent = lambda x: (x[0]==Buy_Coin) and (abs(x[1]-float(Buy_Quantity))/x[1] <= 0.05)
	isWithdrawalCoinAndQuantityWithin5Percent = lambda x: (x[0]==Sell_Coin) and (abs(x[1]-float(Sell_Quantity))/x[1] <= 0.05)
	isDepositOrWithdrawalCoinAndQuantityWithin5Percent	= any(list(map(isDepositCoinAndQuantityWithin5Percent,dailyTxs))+list(map(isWithdrawalCoinAndQuantityWithin5Percent,dailyTxs)))	
	isCoinsMarkets = "coinsmarkets" in Exchange.lower()
	isCryptopia = "cryptopia" in Exchange.lower()
	toSelf = all(map(lambda address:user in address.lower(),[ToAddress,FromAddress]))
	isCoinbaseReward = isCoinbaseDeposit and float(Buy_Quantity)<0.01
	isWithdrawalToSelf = isWithdrawal and toSelf
	hasSmallDepositUSDAmount = False
	# if Buy_Coin=="ENJ":
	# 	for x in icoBook["Buy_Coin"]:
	# 		print(abs(float(x["Buy_Quantity"])-float(Sell_Quantity)) < 0.001)
	# 	exit(1)
	isICOPayment = isWithdrawal and any(map(lambda x:(x["Payment_Date"]==date) and (abs(float(x["Sell_Quantity"])-float(Sell_Quantity)) < 0.001),icoBook[Sell_Coin]))
	isICODistribution = isDeposit and any(map(lambda x:(x["Distribution_Date"]==date) and (abs(float(x["Buy_Quantity"])-float(Buy_Quantity)) < 0.001),icoBook[Buy_Coin]))
	matchInTradeBook = ",".join(listWithoutTaxableColumn) in tradeBook
	if not checkForMissingPriceHistory(isWithdrawal,isDeposit,Buy_Coin,buyCoinUSDValue,Sell_Coin,sellCoinUSDValue,listWithoutTaxableColumn):
		hasSmallDepositUSDAmount = isDeposit and float(Buy_Quantity)*float(buyCoinUSDValue) <= 50.0
		#Conditionals
	if TxID.lower() in txBook:
		taxable = txBook[TxID.lower()]
	elif matchInTradeBook:
		taxable = tradeBook[",".join(listWithoutTaxableColumn)]
	elif isTrade or isPayment:
		taxable = "True"
	elif any([fromPool,toMiningAddress,minedLTC,minedZEC,minedDASH,minedBTC,minedETH]):
		taxable = "Mined"
	elif any([isWithdrawalToPersonalWallet,isDepositFromPersonalWallet,
		boughtWithUSD,usdDeposit,usdWithdrawal,isCoinsMarkets,
		isCryptopia,isWithdrawalToSelf,isICOPayment,isICODistribution,
		toAndFromRecognizedAddress,fromCrowdsale,toCrowdsale]):
		taxable = "False"	
	elif isFork or isAirDropCoin or (hasSmallDepositUSDAmount and not fromRecognizedAddress and notBigCoin):
		taxable = "Airdrop"
	elif isCoinbaseReward:
		taxable = "Coinbase Reward"
	# if Buy_Coin=="ETH":
	# 	print(listWithoutTaxableColumn)
	# 	print("Taxable: {}".format(taxable))
	# 	print("minedETH: {}".format(minedETH))
	# 	input(1)
	listWithoutTaxableColumn.append(taxable)
	return listWithoutTaxableColumn

def fixDates(output):
	newOutputLines = [output[0]]
	for line in output[1:]:
		date = ""
		clock = "00:00:00"
		splitLine = line.split(",")
		newLine = splitLine[:-1]
		dateAndTime = splitLine[-1].split(" ")
		if len(dateAndTime)==2:
			date = dateAndTime[0]
			clock = dateAndTime[1]
		else:
			date = dateAndTime[0]
		dateSections = list(map(int,date.split("-")))
		timeSections = list(map(int,clock.split(":")))
		#timeStruct = time.mktime(tuple(dateSections+timeSections+[0,0,0]))
		newLine.append(time.strftime("%Y-%m-%d %H:%M:%S",tuple(dateSections+timeSections+[0,0,0])))
		newLine = ",".join(newLine)
		newOutputLines.append(newLine)
	return newOutputLines

def getEpochtime(line):
	splitLine = line.split(",")
	date = splitLine[-1]
	if len(date.split(" "))==1:
		date = "{} {}".format(date,"00:00:00")
	return int(time.mktime(time.strptime(date,"%Y-%m-%d %H:%M:%S")))

if len(sys.argv)<2:
	raise AttributeError("Must provide name as first command-line argument")
	exit(1)

user = sys.argv[1]
finalOutputFile = open("{}FinalOutput.csv".format(user),"r").read().split("\n")
finalOutputFile = fixDates(finalOutputFile)
fileHeader = finalOutputFile[0]
finalOutputFile = [fileHeader]+sorted(finalOutputFile[1:],key=getEpochtime,reverse=True)
newFileName = "{}FinalOutputWithChanges.csv".format(user)
finalOutputFileWithChanges = open(newFileName,"w")
print("Started Final Changes Script")
coinSymbolAndTimeToPrice = {}
historicCoinPriceFile = open("HistoricCoinPrices.csv","r")
addressBook = loadAddressBook()
txBook = loadTxBook()
icoBook = loadUserICOBook()
tradeBook = loadTradeBook()
seenCoins = set()
minedOrAirdroppedCoin = set()
for history in historicCoinPriceFile.read().split("\n"):
	#print(history)
	coin,epochtime,price = history.split(",")
	coinSymbolAndTimeToPrice[(coin,int(epochtime))] = price
# 	if price=="Not Found":
# 		seenCoins.add(coin)
# print(seenCoins)
# exit(1)
historicCoinPriceFile.close()
historicCoinPriceFile = open("HistoricCoinPrices.csv","a")
#oldOutputHeader = finalOutputFile[0].split(",")
newOutputLines = ["Action,Buy Quantity,Buy Coin,Sell Quantity,Sell Coin,Exchange,To Address,From Address,TxID,Buy Coin Price (USD),Sell Coin Price (USD),Date,Taxable"]
numRequests = 0
dailyTxs = []
dailyTxsTime = float("inf")
for line in finalOutputFile[-1:0:-1]:
	#Action,Buy Quantity,Buy Coin,Sell Quantity,Sell Coin,Exchange,Deposit Address,Withdrawal Address,TxID,Date
	Action,Buy_Quantity,Buy_Coin,Sell_Quantity,Sell_Coin,Exchange,Deposit_Address,Withdrawal_Address,TxID,Date = line.split(",")
	date = Date.split(" ")[0]
	if date.split("-")[0] == "2017":
		try:
			epochtime = int(time.mktime(time.strptime(date,"%Y-%m-%d")))
		except ValueError:
			print("Date Parse Error: {}".format(line))
			exit(1)
		if dailyTxsTime-epochtime > 86400.0:
			dailyTxs = []
			dailyTxsTime = epochtime
		if Buy_Coin and not Sell_Coin:
			dailyTxs.append((Buy_Coin,float(Buy_Quantity)))
		if Sell_Coin and not Buy_Coin:
			dailyTxs.append((Sell_Coin,float(Sell_Quantity)))
		buyCoinUSDValue = ""
		sellCoinUSDValue = ""
		buyCoinTimeTuple = (Buy_Coin,epochtime)
		sellCoinTimeTuple = (Sell_Coin,epochtime)
		if Buy_Coin:
			if buyCoinTimeTuple not in coinSymbolAndTimeToPrice:
				coinSymbolAndTimeToPrice[buyCoinTimeTuple] = fetchCoinPrice(buyCoinTimeTuple[0],epochtime)
				historicCoinPriceFile.write("\n"+Buy_Coin+","+str(epochtime)+","+str(coinSymbolAndTimeToPrice[buyCoinTimeTuple]))
				numRequests+=1
				print("Request #{}: {}".format(numRequests,buyCoinTimeTuple))
			else:
				pass
				#print("{} already in dictionary".format(buyCoinTimeTuple))
	#hjhhh		if coinSymbolAndTimeToPrice[buyCoinTimeTuple] in ["Not Found","0.0"]
			buyCoinUSDValue = coinSymbolAndTimeToPrice[buyCoinTimeTuple]	
			#print("Price: {}".format(buyCoinUSDValue))		
		if Sell_Coin:
			if sellCoinTimeTuple not in coinSymbolAndTimeToPrice:
				coinSymbolAndTimeToPrice[sellCoinTimeTuple] = fetchCoinPrice(sellCoinTimeTuple[0],epochtime)
				historicCoinPriceFile.write("\n"+Sell_Coin+","+str(epochtime)+","+str(coinSymbolAndTimeToPrice[sellCoinTimeTuple]))			
				numRequests+=1
				print("Request #{}: {}".format(numRequests,sellCoinTimeTuple))
			else:
				pass
				#print("{} already in dictionary".format(sellCoinTimeTuple))
			sellCoinUSDValue = coinSymbolAndTimeToPrice[sellCoinTimeTuple]		
			#print("Price: {}".format(sellCoinUSDValue))
		# print("SELL PRICE: {}".format(sellCoinUSDValue))
		# print("BUY PRICE: {}".format(buyCoinUSDValue))
		if ((sellCoinUSDValue in ["","Not Found"]) and (buyCoinUSDValue in ["","Not Found"])):
			if Buy_Coin:
				minedOrAirdroppedCoin.add("BUY {}: {}".format(buyCoinTimeTuple,buyCoinUSDValue))
			if Sell_Coin:
				minedOrAirdroppedCoin.add("SELL {}: {}".format(sellCoinTimeTuple,sellCoinUSDValue))
		Checked_Deposit_Address = addressBook.get(Deposit_Address.lower()) or Deposit_Address
		Checked_Withdrawal_Address = addressBook.get(Withdrawal_Address.lower()) or Withdrawal_Address
		listWithoutTaxableColumn = [Action,Buy_Quantity,Buy_Coin,Sell_Quantity,Sell_Coin,Exchange,Checked_Deposit_Address,Checked_Withdrawal_Address,TxID,str(buyCoinUSDValue),str(sellCoinUSDValue),date]
		newLineList = addTaxableColumn(seenCoins,dailyTxs,txBook,icoBook,listWithoutTaxableColumn)
		newLine = ",".join(newLineList)
		newOutputLines.append(newLine)
for line in minedOrAirdroppedCoin:
	print("{}".format(line))

# print(len(coinSymbolAndTimeToPrice))
finalOutputFileWithChanges.write("\n".join(newOutputLines[0:1]+newOutputLines[:0:-1]))
print("GENERATED: {}".format(newFileName))