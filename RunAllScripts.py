import sys
import subprocess
import API_Scripts.BittrexAPI_TRANSFERSONLY as BittrexTransfers
import API_Scripts.CryptopiaAPI as Cryptopia
import API_Scripts.KucoinAPI as Kucoin
import API_Scripts.EtherscanAPI as Ethereum
import CSV_Scripts.BittrexTradesToCSV as BittrexTrades
import CSV_Scripts.BinanceFilesToCSV as Binance
import CSV_Scripts.BitstampFileToCSV as Bitstamp
import CSV_Scripts.GeminiFileToCSV as Gemini
import CSV_Scripts.CoinbaseFileToCSV as Coinbase
import CSV_Scripts.ICOFileToCSV as ManualTrades

if len(sys.argv)<2:
	raise AttributeError("Must provide name as first command-line argument")
	exit(1)

user = sys.argv[1]
justCombine = False
if len(sys.argv)>2 and sys.argv[2].lower() == "combine":
	justCombine=True
success = True
modules = []
if user=="Aris":
	pass
	#modules = [BittrexTransfers,Cryptopia,Kucoin,BittrexTrades,Binance,Bitstamp,Gemini,Coinbase,Ethereum]
elif user=="Haig":
	pass
	#modules = [BittrexTransfers,Cryptopia,Kucoin,BittrexTrades,Binance,Gemini,Coinbase,Ethereum,ManualTrades]
else:
	raise ValueError("USER NOT RECOGNIZED")
if not justCombine:
	for module in modules:
		success = success and module.run(user)
if success:
	outputDirectory = "OutputFiles/{}".format(user)
	tradeFileNames = list(map(lambda x:x.decode(), subprocess.check_output(["ls",outputDirectory]).split()))
	outputFilePath = "{}FinalOutput.csv".format(user)
	newOutputFile = open(outputFilePath,"w")
	newOutputContent = ["Action,Buy Quantity,Buy Coin,Sell Quantity,Sell Coin,Exchange,Deposit Address,Withdrawal Address,TxID,Date"]	
	for fileName in tradeFileNames:
		filePath = "{}/{}".format(outputDirectory,fileName)
		for line in open(filePath,"r").read().split("\n"):
			# Make adjustments for Coins which changed Tickers. 
			if "BCC" in line:
				line = line.replace("BCC","BCH") #Bitcoin Cash
			if "ANS" in line:
				line = line.replace("ANS","NEO") #Antshares -> Neo
			newOutputContent.append(line)
	newOutputFile.write("\n".join(newOutputContent))
	print("GENERATED: {}".format(outputFilePath))