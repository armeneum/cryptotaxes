import sys
import subprocess
import requests

user = sys.argv[1]

print(subprocess.check_output(["python3","RunAllScripts.py",user,"combine"]).decode(),end="")
print(subprocess.check_output(["python3","ApplyFinalOutputChanges.py",user]).decode(),end="")
print(subprocess.check_output(["python3","MatchTransactions.py",user]).decode(),end="")