import sys
import time
from collections import defaultdict

def matchingExceptions(t1,t2):
	exceptions = []
	exceptions.append(t1["Exchange"]==t2["Exchange"])
	return exceptions

def duplicateExceptions(t1,t2):
	exceptions = []
	exceptions.append("gemini" in [t1["Exchange"].lower(),t2["Exchange"].lower()])
	exceptions.append("mined" in [t1["Taxable"].lower(),t2["Taxable"].lower()])
	exceptions.append(t1["Exchange"]!=t2["Exchange"])
	return exceptions

user = sys.argv[1]
finalOutputFile = open("{}FinalOutputWithChanges.csv".format(user),"r")
outputFile = open("{}FinalOutputWithChanges2.csv".format(user),"w")
print("Started matching transactions...")
finalOutput = finalOutputFile.read().split("\n")
dateToTransactions = defaultdict(list)
for i,line in enumerate(finalOutput[1:]):
	lineSplit = line.split(",")
	Action,Buy_Quantity,Buy_Coin,Sell_Quantity = lineSplit[:4]
	Sell_Coin,Exchange,To_Address,From_Address = lineSplit[4:8]
	TxID,Buy_Coin_Price,Sell_Coin_Price,Date,Taxable = lineSplit[8:]
	transaction = {
	"Action":Action,
	"Exchange":Exchange,
	"To":To_Address,
	"From":From_Address,
	"TxID":TxID,
	"Sell_Coin":Sell_Coin,
	"Buy_Coin":Buy_Coin,
	"Sell_Quantity":Sell_Quantity,
	"Buy_Quantity":Buy_Quantity,
	"Taxable":Taxable,
	"Epochtime":time.mktime(time.strptime(Date.split(" ")[0],"%Y-%m-%d")),
	"Line":i+1}
	if Action != "Trade":
		dateToTransactions[transaction["Epochtime"]].append(transaction)

#isWithdrawalCoinAndQuantityWithin5Percent = lambda x: (x[0]==Sell_Coin) and (abs(x[1]-float(Sell_Quantity))/x[1] <= 0.05)
duplicates = {}
matches = {}
days = list(dateToTransactions.keys())
for date in days:
	for t1 in dateToTransactions[date]:
		for t2 in dateToTransactions[date]+dateToTransactions[date-86400]:
			if (t1["Line"]!=t2["Line"]) and ("Trade" not in [t1["Action"],t2["Action"]]):
				t1Coin = t1["Buy_Coin"] or t1["Sell_Coin"]
				t1Qty = t1["Buy_Quantity"] or t1["Sell_Quantity"]
				t1Qty = float(t1Qty)
				t2Coin = t2["Buy_Coin"] or t2["Sell_Coin"]
				t2Qty = t2["Buy_Quantity"] or t2["Sell_Quantity"]
				t2Qty = float(t2Qty)
				if not any(matchingExceptions(t1,t2)) and t1["Action"]!=t2["Action"] and (t1["Line"] not in matches and t2["Line"] not in matches):
					if (t1Coin==t2Coin) and (abs(t1Qty-t2Qty)/t2Qty < 0.05):
						#print("Matched Transactions")
						matches[t1["Line"]] = t2["Line"]
						matches[t2["Line"]] = t1["Line"]
						# print("T1: {}".format(t1))
						# print("T2: {}".format(t2))
						# print("Matched line {} with line {}".format(t1["Line"],t2["Line"]))
						# print("Same TX: {}".format(t1["TxID"]==t2["TxID"]))
				elif not any(duplicateExceptions(t1,t2)) and t1["Action"]==t2["Action"] and (t1["Line"] not in duplicates and t2["Line"] not in duplicates):
					if (t1Coin==t2Coin) and (abs(t1Qty-t2Qty)/t2Qty < 0.05):
						#print("Duplicate Transactions")
						duplicates[t1["Line"]] = t2["Line"]
						duplicates[t2["Line"]] = t1["Line"]
						# print("T1: {}".format(t1))
						# print("T2: {}".format(t2))
						# print("Line {} is duplicate of line {}".format(t1["Line"],t2["Line"]))
						# print("Same TX: {}".format(t1["TxID"]==t2["TxID"]))
showDuplicates = True
showMatches = False

# for a in duplicates:
# 	b = duplicates[a]
# 	lineA = finalOutput[a].split(",")
# 	lineB = finalOutput[b].split(",")
# 	if showDuplicates:
# 		finalOutput[a] = ",".join(lineA[:-1]+["Duplicate of Line {}".format(b+1)])
# 		finalOutput[b] = ",".join(lineB[:-1]+["Duplicate of Line {}".format(a+1)])
# 	else:
# 		finalOutput[a] = ",".join(lineA[:-1]+["FALSE"])
# 		finalOutput[b] = ",".join(lineB[:-1]+["FALSE"])
for a in matches:
	b = matches[a]
	lineA = finalOutput[a].split(",")
	lineB = finalOutput[b].split(",")
	if showMatches:
		finalOutput[a] = ",".join(lineA[:-1]+["Matches Line {}".format(b+1)])
		finalOutput[b] = ",".join(lineB[:-1]+["Matches Line {}".format(a+1)])
	else:
		finalOutput[a] = ",".join(lineA[:-1]+["FALSE"])
		finalOutput[b] = ",".join(lineB[:-1]+["FALSE"])
outputFile.write("\n".join(finalOutput))
print("GENERATED: {}".format("{}FinalOutputWithChanges2.csv".format(user)))