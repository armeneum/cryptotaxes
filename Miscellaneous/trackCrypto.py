import sys
from collections import defaultdict
from collections import Counter

try:
	user = sys.argv[1]
except IndexError:
	print("ERROR: Must Enter User as Command-Line Argument.")
	exit(1)

try:
	transactionHistory = open("{}FinalOutputWithChanges.csv".format(user),"r").read().split("\n")
except FileNotFoundError:
	print("ERROR: {}FinalOutputWithChanges.csv Not Found.".format(user))
	exit(1)

wallets = defaultdict(Counter)
txs = set()
for transaction in transactionHistory[1:]:
	lineSplit = transaction.split(",")
	Action,Buy_Quantity,Buy_Coin,Sell_Quantity = lineSplit[:4]
	Sell_Coin,Exchange,To_Address,From_Address = lineSplit[4:8]
	TxID,Buy_Coin_Price,Sell_Coin_Price,Date,Taxable = lineSplit[8:]
	if TxID not in txs:
		if Action=="Deposit":
			#print("Deposit")
			#print("TO: {}".format(To_Address))
			wallets[To_Address][Buy_Coin]+=float(Buy_Quantity)
			wallets[From_Address][Buy_Coin]-=float(Buy_Quantity)
		elif Action=="Withdrawal":
			#print("Withdrawal")
			#print("FROM: {}".format(From_Address))
			wallets[To_Address][Sell_Coin]+=float(Sell_Quantity)
			wallets[From_Address][Sell_Coin]-=float(Sell_Quantity)
		txs.add(TxID)
for wallet in wallets:
	# if wallet:
	print(wallet)
	for coin in sorted(wallets[wallet]):
		# if wallets[wallet][coin] < -0.01 or wallets[wallet][coin] > 0.1:
		print("{}{}: {:,.2f}".format("\t",coin,wallets[wallet][coin]))
	print("")