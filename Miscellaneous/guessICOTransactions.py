import sys
import requests
user = sys.argv[1]
finalOutputWithUSD = open("{}FinalOutputWithChanges.csv".format(user)).read().split("\n")

for line in finalOutputWithUSD:
	Action,Buy_Quantity,Buy_Coin,Sell_Quantity,Sell_Coin,Exchange,Deposit_Address,Withdrawal_Address,TxID,Buy_Coin_Price_USD,Sell_Coin_Price_USD,Date = line.split(",")
	date = Date.split(" ")[0]
	year = date.split("-")[0]
	if year=="2017" and Sell_Coin == "ETH" and Exchange=="Personal Wallet":
		print(line.split(","))
		ABI_BASE_URL = "https://api.etherscan.io/api?module=contract&action=getabi&address="
		addresses = {"Deposit":Deposit_Address, "Withdrawal":Withdrawal_Address}
		for action in addresses:
			if Action==action:
				r = requests.get(ABI_BASE_URL+addresses[action])
				print(r.json())