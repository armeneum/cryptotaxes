import sys
import requests

user = sys.argv[1]
cryptoTaxReport = open("{}CryptoTaxReport.csv".format(user),"r").readlines()
portfolio = defaultdict(float)

for line in cryptoTaxReport:
	Date,Buy_Quantity,Buy_Coin,Buy_Coin_Price,Sell_Quantity,Sell_Coin,Sell_Coin_Price = line.strip().split(",")
	if Sell_Coin == "USD":
		dollarsSold = float(Sell_Quantity)*float(Sell_Coin_Price)
		fractionOfTotal = dollarsSold/portfolioValue(Date)
	if Buy_Coin:
		portfolio[Buy_Coin] += float(Buy_Quantity)
	if Sell_Coin:
		portfolio[Sell_Coin] -= float(Sell_Quantity)

def portfolioValue(date):
	epochtime = time.mktime(time.strptime(date,"%Y-%m-%d"))
	total = 0.0
	for coin in portfolio:
		price = fetchCoinPrice(coin,epochtime)
		total += (portfolio[coin]*price)

def fetchCoinPrice(coinSymbol,epochtime=int(time.time())):
	apiURL = "https://min-api.cryptocompare.com/data/pricehistorical?fsym={}&tsyms=USD&ts={}".format(coinSymbol,epochtime)
	r = requests.get(apiURL)
	print(r.json())
	coinPrice = r.json().get(coinSymbol)
	if coinPrice and coinPrice["USD"]!=0:
		return coinPrice["USD"]
	return "Not Found"