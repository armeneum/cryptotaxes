# Action,Buy Quantity,Buy Coin,Sell Quantity,Sell Coin,,,Exchange,,,Date,Buy Coin Price (USD),Sell Coin Price (USD)
# Trade,0.0148,ETH,19.72,GVT,,,Binance,,,2018-01-01 20:10:16,756.2,11.19
# Trade,0.0148,ETH,16.72,GVT,,,Binance,,,2018-01-01 19:53:28,756.2,11.19
# Trade,0.0148,ETH,50,GVT,,,Binance,,,2018-01-01 19:49:44,756.2,11.19
import sys
import time
from collections import Counter as c
from collections import defaultdict
user = sys.argv[1]
numSells = None
stopCoin = None
if len(sys.argv)>2:
	try:
		numSells = int(sys.argv[2])
	except ValueError:
		stopCoin = sys.argv[2]

finalOutputWithUSD = open("{}FinalOutputWithUSD.csv".format(user,user)).read().split("\n")

# balances = c()
# for line in finalOutputWithUSD[1:]:
# 	Action,Buy_Quantity,Buy_Coin,Sell_Quantity,Sell_Coin,garb1,garb2,Exchange,garb3,garb4,Date,Buy_Coin_Price_USD,Sell_Coin_Price_USD = line.split(",")
# 	if Buy_Coin:
# 		balances[Buy_Coin] += float(Buy_Quantity)
# 	if Sell_Coin:
# 		balances[Sell_Coin] -= float(Sell_Quantity)
# for coin in list(filter(lambda x:balances[x]<-0.005 or balances[x]>0.005,sorted(balances,key=lambda x:-balances[x]))):
# 	print("{}: {}".format(coin,balances[coin]))

sells = []
buyCoins = defaultdict(list)

for line in finalOutputWithUSD[1:]:
	Action,Buy_Quantity,Buy_Coin,Sell_Quantity,Sell_Coin,garb1,garb2,Exchange,garb3,garb4,Date,Buy_Coin_Price_USD,Sell_Coin_Price_USD = line.split(",")
	date = Date.split(" ")[0]
	year = date.split("-")[0]
	if Action=="Trade" and year=="2017":
		epochtime = int(time.mktime(time.strptime(date,"%Y-%m-%d")))
		buyCoins[Buy_Coin].append({"Amount": float(Buy_Quantity), "Price": float(Buy_Coin_Price_USD), "Epochtime": epochtime})
		if Sell_Coin not in ["USD","USDT"]:
			sells.append({"Coin":Sell_Coin, "Amount": float(Sell_Quantity), "Price": float(Sell_Coin_Price_USD), "Epochtime": epochtime})

for coin in buyCoins:
	buyCoins[coin].sort(key=lambda x: -int(x["Epochtime"]))
sells.sort(key=lambda x: int(x["Epochtime"]))

taxable = []
incomeTaxable = c()
sellCounter = 0
for sold in sells:
	print("\n\n\nNew Sell: {}\n".format(sold))
	loopCounter = 0
	if sold["Coin"] != "USD":
		buys = buyCoins[sold["Coin"]]
		# print(sold)
		# print(buys)
		# print("\n\n\n")
		amountLeft = sold["Amount"]
		sellCounter+=1
		while amountLeft > 0.00001:
			loopCounter += 1
			if buys:
				firstOutAmountBought = 0
				print("AmountLeft: {}".format(amountLeft))
				print("Number of {} Buys Left: {}".format(sold["Coin"],len(buys)))
				print("Furthest Buy: {}".format(buys[-1]))
				#print("FurthestBuy: {}".format(buys[-1]))
				firstOutPriceBought = buys[-1]["Price"]
				if amountLeft <= buys[-1]["Amount"]:
					buys[-1]["Amount"] -= amountLeft
					firstOutAmountBought = amountLeft
					amountLeft = 0
					#print("FurthestBuy: {}".format(buys[-1]))
				else:
					print("AmountLeft = {} - {} = {}".format(amountLeft,buys[-1]["Amount"],amountLeft-buys[-1]["Amount"]))
					firstOutAmountBought = buys.pop()["Amount"]
					amountLeft -= firstOutAmountBought
				profitPerCoin = sold["Price"]-firstOutPriceBought
				totalProfit = profitPerCoin * firstOutAmountBought
				print("\nTAXABLE: {} sold for (${:,.2f} - ${:,.2f} = ${:,.2f} profit per coin) * {:,} coins = ${:,.2f} total taxable profit".format(
					sold["Coin"],
					sold["Price"],
					firstOutPriceBought,
					profitPerCoin,
					firstOutAmountBought,
					totalProfit))
				if amountLeft==0:
					print("\nAmountLeft After: {}".format(amountLeft))
					print("Number of Buys Left After: {}".format(len(buys)))
					print("Furthest Buy After: {}".format(buys[-1]))
				taxable.append((sold["Coin"],totalProfit))
				print("\n{}".format(taxable[-loopCounter:]))
				print("\n")
			else:
				print(sold)
				incomeTaxable[sold["Coin"]]+=(sold["Price"] * amountLeft)
				print("Income Taxable: {}".format(incomeTaxable[sold["Coin"]]))
				amountLeft = 0
			if stopCoin and stopCoin==sold["Coin"]:
				print("ALL BUYS: {}".format(buys))
				input()
		if numSells and sellCounter==numSells:
			print("Income Tax: {}".format(incomeTaxable))
			print("Taxable: {}".format(taxable))
			exit(1)

print("\n\n\n\n\nIncome Tax: {}".format(incomeTaxable))
print("Taxable: {}\n\n\n\n\n".format(taxable))

capitalGainsRate = .40
incomeRate = .40

capitalGains = 0
incomeTax = 0
for coin in taxable:
	# if coin[1] > 10000:
	# 	print(coin)
	capitalGains += coin[1]*capitalGainsRate
for coin in incomeTaxable:
	incomeTax += incomeTaxable[coin]*incomeRate

print("Capital Gains Tax Owed: ${:,.2f}".format(capitalGains))
print("Income Tax Owed: ${:,.2f}".format(incomeTax))