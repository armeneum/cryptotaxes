import sys
import requests
user = sys.argv[1]
finalOutputWithUSD = open("{}FinalOutputWithUSD.csv".format(user)).read().split("\n")

for line in finalOutputWithUSD:
	Action,Buy_Quantity,Buy_Coin,Sell_Quantity,Sell_Coin,Exchange,Deposit_Address,Withdrawal_Address,TxID,Buy_Coin_Price_USD,Sell_Coin_Price_USD_,Date = line.split(",")
	date = Date.split(" ")[0]
	year = date.split("-")[0]
	if year=="2017" and Action=="Withdrawal" and Sell_Coin == "ETH" and float(Sell_Quantity) > 0.5 and Exchange=="Personal Wallet":
		print(line)