import sys

user = sys.argv[1]
transactionLines = open("{}FinalOutputWithChanges2.csv".format(user),"r").readlines()

print("Date,Buy Quantity,Buy Coin,Buy Coin Price (USD),Sell Quantity,Sell Coin,Sell Coin Price (USD)")
for transaction in transactionLines[1:]:
	Action,Buy_Quantity,Buy_Coin,Sell_Quantity,Sell_Coin,Exchange,To_Address,From_Address,TxID,Buy_Coin_Price,Sell_Coin_Price,Date,Taxable = transaction.strip().split(",")
	Taxable = Taxable.lower()
	Buy_Quantity = float(Buy_Quantity) if Buy_Quantity else ""
	Sell_Quantity = float(Sell_Quantity) if Sell_Quantity else ""
	Buy_Coin_Price = float(Buy_Coin_Price) if Buy_Coin_Price not in ["","Not Found"] else ""
	Sell_Coin_Price = float(Sell_Coin_Price) if Sell_Coin_Price not in ["","Not Found"] else ""
	#print({"Action":Action, "Taxable":Taxable})
	#print(transaction.strip())
	if Action == "Trade" and Taxable == "true":
		if Buy_Coin_Price in ["","Not Found"]:
			#print("{} {} {}".format(Sell_Coin_Price,Sell_Quantity,Buy_Quantity))
			Buy_Coin_Price = (Sell_Coin_Price*Sell_Quantity)/Buy_Quantity
		if Sell_Coin_Price in ["","Not Found"]:
			Sell_Coin_Price = (Buy_Coin_Price*Buy_Quantity)/Sell_Quantity
		print(",".join(map(str,[Date,Buy_Quantity,Buy_Coin,Buy_Coin_Price,Sell_Quantity,Sell_Coin,Sell_Coin_Price])))
	if Action == "Deposit" and Taxable in ["mined","airdrop"]:
		if not Buy_Coin_Price:
			Buy_Coin_Price = 0.0
		print(",".join(map(str,[Date,Buy_Quantity,Buy_Coin,Buy_Coin_Price,Sell_Quantity,Sell_Coin,Sell_Coin_Price])))
	if Action == "Withdrawal" and Taxable == "true":
		Buy_Coin = "USD"
		Buy_Coin_Price = 1
		Buy_Quantity = Sell_Quantity*Sell_Coin_Price
		print(",".join(map(str,[Date,Buy_Quantity,Buy_Coin,Buy_Coin_Price,Sell_Quantity,Sell_Coin,Sell_Coin_Price])))