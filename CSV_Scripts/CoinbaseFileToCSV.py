import re
import sys

def run(user):
	exchange = "Coinbase"
	print("Started {} script.".format(exchange))
	try:
		inputFileDirectory = "UserInputFiles/{}/{}".format(user,exchange)
		inputFileLines = open("{}/{}{}Report.csv".format(inputFileDirectory,user,exchange),"r").read().split("\n")
		outputFileName = "OutputFiles/{}/{}{}Output.csv".format(user,user,exchange)
		outputFile = open(outputFileName,"w")
		outputLines = []
		for line in inputFileLines:
			lineSplits = line.split(",")
			if len(lineSplits)>=6:
				action = lineSplits[1]
				if action=="Buy":
					#04/20/2017,Buy,BTC,1.0,1242.32,1260.83,"","Bought 1.0000 BTC for $1,260.83 USD.
					date,action,coin,quantity,price,priceWithFee = lineSplits[:6]
					action = "Trade"
					date = date.split("/")
					date = "-".join([date[-1],date[0],date[1]])
					buyQuantity = quantity
					sellQuantity = priceWithFee
					buyCoin = coin
					sellCoin = "USD"
					outputLines.append("{},{},{},{},{},{},{},{},{},{}".format(action,buyQuantity,buyCoin,sellQuantity,sellCoin,exchange,"","","",date))
				elif action=="Sell":
					#ONLY BUY SUPPORTED FOR NOW
					raise ValueError("ONLY BUY SUPPORTED FOR NOW")
				elif action=="Receive":
					#10/20/2017,Receive,BTC,0.02015011,5670.31,114.26,"","Received from an external account
					date,action,coin,quantity = lineSplits[:4]
					date = date.split("/")
					date = "-".join([date[-1],date[0],date[1]])	
					action = "Deposit"
					txID = "Unavailable"
					toAddress = "Exchange Wallet"
					fromAddress = "Unavailable"
					outputLines.append("{},{},{},{},{},{},{},{},{},{}".format(action,quantity,coin,"","",exchange,toAddress,fromAddress,txID,date))
				elif action=="Send":
					#08/03/2017,Send,BTC,1.28377799,2729.92,3504.61,19yKDQbz6bZzWxbpMTqDgpvo7SBfADprcW,"Sent to 19yKDQbz6bZzWxbpMTqDgpvo7SBfADprcW
					#11/09/2017,Send,BTC,0.42802552,7196.97,3080.49,1MhuMkVg5MEGL2UKguBL4o9kEqSERhotpe,"Sent to 1MhuMkVg5MEGL2UKguBL4o9kEqSERhotpe
					date,action,coin,quantity = lineSplits[:4]
					date = date.split("/")
					date = "-".join([date[-1],date[0],date[1]])
					action = "Withdrawal"
					txID = "Unavailable"
					toAddress = lineSplits[6]
					fromAddress = "Exchange Wallet"
					outputLines.append("{},{},{},{},{},{},{},{},{},{}".format(action,"","",quantity,coin,exchange,toAddress,fromAddress,txID,date))			
			else:
				#Line ignored
				pass
		outputFile.write("\n".join(outputLines))
		print("GENERATED: {}".format(outputFileName))
		return True
	except FileNotFoundError:
		print("ERROR: {}'s UserInputFile(s) for {} were not found.".format(user,exchange))
		return False

if(len(sys.argv) > 2 and sys.argv[2]=="run"):
	run(sys.argv[1])