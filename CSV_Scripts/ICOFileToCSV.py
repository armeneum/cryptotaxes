import sys

def run(user):
	print("Started ICO/Manual Trade script.")
	icoList = open("UserInputFiles/{}/ManualTransactions/{}ICOList.csv".format(user,user),"r").readlines()
	icoLines = []
	manualTrades = open("UserInputFiles/{}/ManualTransactions/{}ManualTrades.csv".format(user,user),"r").read().split("\n")
	outputFileName = "OutputFiles/{}/{}ManualTradesOutput.csv".format(user,user)
	outputFile = open(outputFileName,"w")
	for line in icoList[1:]:
		Payment_Date,Distribution_Date,Sell_Quantity,Sell_Coin,Buy_Quantity,ICO_Coin = line.rstrip().split(",")
		icoLines.append("Trade,{},{},{},{},,,,,{}".format(Buy_Quantity,ICO_Coin,Sell_Quantity,Sell_Coin,Payment_Date))
	outputFile.write("\n".join(manualTrades+icoLines))
	print("GENERATED: {}".format(outputFileName))

if len(sys.argv) == 3 and sys.argv[2] == "run":
	user = sys.argv[1]
	run(user)
else:
	print("USAGE: python ICOFileToCSV.py <user> <run>")