from datetime import datetime
from dateutil import tz
import requests
import re
import sys

def UtcDatetimeToEst(utc):
	from_zone = tz.gettz('UTC')
	to_zone = tz.gettz('America/New_York')
	utc = datetime.strptime(utc, '%Y-%m-%d %H:%M:%S')
	utc = utc.replace(tzinfo=from_zone)
	eastern = utc.astimezone(to_zone)
	return str(eastern)[:-6]

def cleanTransactionLine(messyTransaction):
	#2017-07-05,23:08:28.809,Buy,BTCUSD,Limit,Taker,25.00 ,"($6,641.75)",($16.60),"$14,343.28 ",2.55052698 BTC ,,2.5505784 BTC ,,0.0 ETH ,883492724,883492712,2017-07-05,23:08:28.806,,,,,,
	openQuote = False
	newLetters = []
	for letter in messyTransaction:
		if letter == "\"":
			openQuote = not openQuote
		elif openQuote and letter == ',':
			#Don't Add
			pass
		elif letter in '"()$':
			#Don't Add
			pass
		else:
			newLetters.append(letter)
	return "".join(newLetters)

def getTradeCSV(exchange,transactions):
	lines = []
	amountIndices = {"USD":7,"BTC":10,"ETH":13}
	transactions = list(filter(lambda x:x,transactions))
	for messyTransaction in transactions:
		transaction = cleanTransactionLine(messyTransaction)
		data = list(map(str.strip,transaction.split(",")))
		Date,Time,PurchaseType,Coins,TransferType = data[:5]
		Time = Time[:-4]
		easternDate = UtcDatetimeToEst("{} {}".format(Date,Time))
		quantity,buyQuantity,sellQuantity = 0,0,0
		txID = data[-4]
		if PurchaseType == "Credit":
			action = "Deposit"
			#2017-05-07,15:49:07.701,Credit,USD,Deposit (Instant ACH Transfer),,,$500.00 ,,$500.00 ,,,0.0 BTC ,,0.0 ETH ,,,,,,,,,,
			#2017-06-21,01:03:44.162,Credit,ETH,Deposit (ETH),,,,,$637.29 ,,,1.5 BTC ,1.00054524506236 ETH ,19.0005452450624 ETH ,,,,,,,984af532717dea2840cf115112950d36d3233b66fb01b95a152991ea56da8e4d,,,
			#2017-09-22,19:38:25.924,Credit,USD,Deposit (Wire Transfer),,,"$9,000.00 ",,"$9,000.85 ",,,0.72245925 BTC ,,24.1178462287067 ETH ,,,,,,,,,,
			quantity = data[amountIndices[Coins]].split(" ")[0]
			toAddress = "Exchange Wallet"
			fromAddress = "Unavailable"
			# if Coins=="ETH":
			# 	r = requests.get("https://api.etherscan.io/api?module=proxy&action=eth_getTransactionByHash&txhash={}".format(txID))
			# 	#print(r.json())
			# 	fromAddress = r.json()["result"]["from"]
			newLine = "{},{},{},{},{},{},{},{},{},{}".format(action,quantity,Coins,"","",exchange,toAddress,fromAddress,txID,easternDate)
		elif PurchaseType=="Debit":
			action = "Withdrawal"
			#2017-10-24,22:38:07.912,Debit,ETH,Withdrawal (ETH),,,,,$0.03 ,,,0.49999188 BTC ,(24.114 ETH),0.00384622870668779 ETH ,,,,,,,a945bbb5b5723640d7dd82856a81a0a05dd71f5159d033efd54f2e25e500d2af,,0x3a987c78b147acd2925660e504d0635801c704e5,
			quantity = data[amountIndices[Coins]].split(" ")[0]
			toAddress = data[-2]
			fromAddress = "Exchange Wallet"
			if Coins=="ETH" and txID:
				txID = "0x"+txID
			newLine = "{},{},{},{},{},{},{},{},{},{}".format(action,"","",quantity,Coins,exchange,toAddress,fromAddress,txID,easternDate)			
		elif PurchaseType in ["Buy","Sell"]:
			buy = PurchaseType=="Buy"
			action = "Trade"
			buyCoin = data[3][:3] if buy else data[3][3:]
			sellCoin = data[3][3:] if buy else data[3][:3]
			buyQuantity = data[amountIndices[buyCoin]].split(" ")[0]
			sellQuantity = data[amountIndices[sellCoin]].split(" ")[0]
			newLine = "{},{},{},{},{},{},{},{},{},{}".format(action,buyQuantity,buyCoin,sellQuantity,sellCoin,exchange,"","","",easternDate)
		# newLineSplit = newLine.split(",")
		# if newLineSplit[0] == "Trade" and not newLineSplit[1]:
		# 	print(messyTransaction)
		# 	print(transaction)
		# 	input("{}: {}".format(newLine,len(newLine.split(","))))
		lines.append(newLine)
	return "\n".join(lines)

def run(user):
	exchange = "Gemini"
	try:
		print("Started {} script.".format(exchange))
		inputFileDirectory = "UserInputFiles/{}/{}".format(user,exchange)
		inputFileLines = open("{}/{}GeminiReport.csv".format(inputFileDirectory,user),"r").read().split("\n")
		inputFileLines = inputFileLines[1:-2]
		outputFileName = "OutputFiles/{}/{}{}Output.csv".format(user,user,exchange)
		outputFile = open(outputFileName,"w")
		outputFile.write(getTradeCSV(exchange,inputFileLines))
		print("GENERATED: {}".format(outputFileName))
		return True
	except FileNotFoundError:
		print("ERROR: {}'s UserInputFile(s) for {} were not found.".format(user,exchange))
		return False

if(len(sys.argv) > 2 and sys.argv[2]=="run"):
	run(sys.argv[1])