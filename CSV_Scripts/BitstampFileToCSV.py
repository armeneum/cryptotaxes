import sys
import re
import time

def run(user=None):
	if not user: user = sys.argv[1]
	exchange = "Bitstamp"
	toAddress = "Unavailable"
	fromAddress = "Unavailable"
	txID = "Unavailable"
	print("Started {} script.".format(exchange))
	inputFileDirectory = "UserInputFiles/{}/{}".format(user,exchange)
	try:
		inputFile = open("{}/{}{}Report.csv".format(inputFileDirectory,user,exchange))
		outputFileName = "OutputFiles/{}/{}{}Output.csv".format(user,user,exchange)
		outputFile = open(outputFileName,"w")
		outputLines = []
		# Type,Datetime,Account,Amount,Value,Rate,Fee,Sub Type
		# Deposit,"May. 25, 2017, 12:06 PM",Main Account,2000.00 USD,,,7.50 USD,
		# Market,"May. 27, 2017, 02:48 PM",Main Account,2428.18889971 XRP,498.75 USD,0.20540 USD,1.25 USD,Buy
		# Market,"May. 27, 2017, 02:50 PM",Main Account,2385.15317765 XRP,491.27 USD,0.20597 USD,1.23 USD,Buy
		# Market,"May. 27, 2017, 04:18 PM",Main Account,4442.01995012 XRP,997.50 USD,0.22456 USD,2.50 USD,Buy
		# Withdrawal,"Aug. 20, 2017, 08:24 PM",Main Account,9255.36202700 XRP,,,,
		 
		# Withdrawal,,,55,ETH,Binance,0x5411A78d08817906d2320B90eC5F3367A597A42a,Exchange Wallet,0x9d8eb8c698cbe3d0c12867546f67a4b42bf19de9062c751aabc0d55a0505a04a,2018-01-15 18:50:00
		# Deposit,699.99954,GVT,,,Binance,Exchange Wallet,0x0f85c94acf85eb878814003e1122e4632fd81948,0x0bc0a6350ae8f794a5386592607e9eec518f77687e07e3ae781a63b7f6a250e2,2018-01-29 11:36:29
		# Trade,0.0091,ETH,225,GVT,Binance,,,,2017-12-17 07:44:08
		for line in inputFile.read().split("\n")[1:]:
			if line:
				action,Datetime,Account,Amount,Value,Rate,Fee,Sub_Type = re.match("(.*),\"(.*)\",(.*),(.*),(.*),(.*),(.*),(.*)",line).groups()
				date = time.strftime("%Y-%m-%d %H:%M:%S",time.strptime(Datetime,"%b. %d, %Y, %I:%M %p"))
				if action == "Deposit":
					quantity,coin = Amount.split(" ")
					outputLines.append("{},{},{},{},{},{},{},{},{},{}".format(action,quantity,coin,"","",exchange,toAddress,fromAddress,txID,date))
				elif action == "Withdrawal":
					quantity,coin = Amount.split(" ")
					outputLines.append("{},{},{},{},{},{},{},{},{},{}".format(action,"","",quantity,coin,exchange,toAddress,fromAddress,txID,date))			
				else:
					action = "Trade"
					buyQuantity,buyCoin = Amount.split(" ")
					sellQuantity,sellCoin = Value.split(" ")
					outputLines.append(",".join([action,buyQuantity,buyCoin,sellQuantity,sellCoin,exchange,"","","",date]))
		outputFile.write("\n".join(outputLines))
		print("GENERATED: {}".format(outputFileName))
		return True
	except FileNotFoundError:
		print("ERROR: {}'s UserInputFile(s) for {} were not found.".format(user,exchange))
		return False

if(len(sys.argv) > 2 and sys.argv[2]=="run"):
	run(sys.argv[1])