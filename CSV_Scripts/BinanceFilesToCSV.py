import re
import subprocess
import sys

def getTrades(inputFileDirectory):
	tradeFileNames = list(filter(
			lambda x:(all(word in x for word in ["csv","Trade"])),
			map(lambda x:x.decode(),
				subprocess.check_output(["ls",inputFileDirectory]).split())))
	trades = []
	for file in tradeFileNames:
		trades.extend(open("{}/{}".format(inputFileDirectory,file),"r").read().split("\n")[1:])
	return trades

def getTradeCSV(exchange,trades):
	lines = []
	action = "Trade"
	for trade in trades:
		if len(trade.strip())>0:
			date,Market,Type,Price,Amount,Total,Fee,Fee_Coin = trade.split(",")
			buy = "BUY" == Type
			buyQuantity,sellQuantity = [Amount,Total] if buy else [Total,Amount]
			buyCoin,sellCoin = (Market[:-3],Market[-3:]) if buy else (Market[-3:],Market[:-3])
			lines.append("{},{},{},{},{},{},{},{},{},{}".format(action,buyQuantity,buyCoin,sellQuantity,sellCoin,exchange,"","","",date))
	return "\n".join(lines)

def getTransersCSV(deposits,withdrawals):
	lines = []
	exchange = "Binance"
	transfers = {"Deposit":deposits,"Withdrawal":withdrawals}
	for action in transfers:
		for transfer in transfers[action]:
			if len(transfer)>0:
				date,coin,quantity,Address,txID,Status = transfer.split(",")
			if Status=="Completed":
				if action=="Deposit":
					#print(transfer)
					toAddress = "Exchange Wallet"
					fromAddress = Address
					lines.append("{},{},{},{},{},{},{},{},{},{}".format(action,quantity,coin,"","",exchange,toAddress,fromAddress,txID,date))
				else:
					toAddress = Address
					fromAddress = "Exchange Wallet"
					lines.append("{},{},{},{},{},{},{},{},{},{}".format(action,"","",quantity,coin,exchange,toAddress,fromAddress,txID,date))			
	return "\n".join(lines)

def run(user=None):
	if not user: user = sys.argv[1]
	exchange = "Binance"
	print("Started {} script.".format(exchange))
	inputFileDirectory = "UserInputFiles/{}/{}".format(user,exchange)
	try:
		deposits = open("{}/{}{}Deposits.csv".format(inputFileDirectory,user,exchange),"r").read().split("\n")[1:]
		withdrawals = open("{}/{}{}Withdrawals.csv".format(inputFileDirectory,user,exchange),"r").read().split("\n")[1:]
		trades = getTrades(inputFileDirectory)
		outputFileName = "OutputFiles/{}/{}{}Output.csv".format(user,user,exchange)
		outputFile = open(outputFileName,"w")
		tradeCSV = getTradeCSV(exchange,trades)
		transfersCSV = getTransersCSV(deposits,withdrawals)
		outputFile.write(tradeCSV+"\n"+transfersCSV)
		print("GENERATED: {}".format(outputFileName))
		return True
	except FileNotFoundError:
		print("ERROR: {}'s UserInputFile(s) for {} were not found.".format(user,exchange))
		return False

if(len(sys.argv) > 2 and sys.argv[2]=="run"):
	run(sys.argv[1])