import re
import sys

def run(user):
	exchange = "Bittrex"
	print("Started {} script.".format(exchange))
	try:
		bittrexFullOrders = open("UserInputFiles/{}/{}/{}{}Trades.csv".format(user,exchange,user,exchange),"r",encoding='utf-16-le')#may need to exclude encoding parameter on certain machines
		outputFileName = "OutputFiles/{}/{}{}TradesOutput.csv".format(user,user,exchange)
		outputFile = open(outputFileName,"w")
		lines = []
		action = "Trade"
		# 9e98ef6d-1625-4d8a-98ff-47147374f1dd,
		# ETH-LTC,
		# LIMIT_SELL,
		# 0.20000000,
		# 0.45574763,
		# 0.00022787,
		# 0.09114952,
		# 12/17/2017 6:33:55 AM,
		# 12/17/2017 6:54:55 AM
		for trade in bittrexFullOrders.readlines()[1:]:
			#print(trade.split(","))
			OrderUuid,Exchange,Type,Quantity,Limit,CommissionPaid,Price,Opened,Closed = trade.split(',')
			buy = "BUY" in Type
			buyQuantity,sellQuantity = [Quantity,Price] if buy else [Price,Quantity]
			buyCoin,sellCoin = Exchange.split("-")[::-1] if buy else Exchange.split("-")
			dateOriginalFormat = Closed[:-1] # 12/17/2017 6:54:55 AM
			#print(dateOriginalFormat)
			month,day,year,hour,minute,second,period = re.match('(.*)/(.*)/(.*) (.*):(.*):(.*) (.*)',dateOriginalFormat).groups()
			hour = hour if period=="AM" else str((int(hour)+12)%24)
			date = "{}-{}-{} {}:{}:{}".format(year,month,day,hour,minute,second) #2017-12-17 19:09:14
			# print("Before: {}".format(dateOriginalFormat))	
			# print("After: {}".format(date))
			lines.append("{},{},{},{},{},{},{},{},{},{}".format(action,buyQuantity,buyCoin,sellQuantity,sellCoin,exchange,"","","",date))
		outputFile.write("\n".join(lines))
		print("GENERATED: {}".format(outputFileName))
		return True
	except FileNotFoundError:
		print("ERROR: {}'s UserInputFile(s) for {} were not found.".format(user,exchange))
		return False

if(len(sys.argv) > 2 and sys.argv[2]=="run"):
	run(sys.argv[1])