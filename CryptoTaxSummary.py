import sys
import time
from collections import defaultdict
from collections import Counter
from functools import reduce

def loadTransactionsFromDate(epochtime,	buyCoins,income):
	for transaction in dateToTransactions[epochtime]:
		if transaction["Taxable"].lower() in ["airdrop","fork","mined"]:
			#IF AIRDROP,FORKED,OR MINED
			if transaction["Action"] == "Withdrawal":
				tranCopy = transaction.copy()
				tranCopy["Action"]=="Deposit"
				tranCopy["Buy_Coin"]=tranCopy["Sell_Coin"]
				tranCopy["Buy_Quantity"] = tranCopy["Sell_Quantity"]
				tranCopy["Buy_Coin_Price"] = 0.0 or tranCopy["Sell_Coin_Price"]
				income.append(tranCopy)
				if logger:
					print("Mined {} {} on {}".format(tranCopy["Sell_Quantity"],tranCopy["Sell_Coin"],tranCopy["Date"]))
				buyCoins[tranCopy["Sell_Coin"]].append(tranCopy)
				portfolio[tranCopy["Sell_Coin"]] += tranCopy["Buy_Quantity"]
			elif transaction["Action"] == "Deposit":
				transaction["Buy_Coin_Price"] = transaction["Buy_Coin_Price"] or 0.0
				income.append(transaction)
				if logger:	
					print("Mined {} {} on {}".format(transaction["Buy_Quantity"],transaction["Buy_Coin"],transaction["Date"]))
				buyCoins[transaction["Buy_Coin"]].append(transaction)
				portfolio[transaction["Buy_Coin"]] += transaction["Buy_Quantity"]
			else:
				raise ValueError("Trade cannot be an Airdrop/Fork")
		elif transaction["Action"]=="Withdrawal" and transaction["Taxable"].lower()=="true":
			#IF NON-ICO PAYMENT
			#DOES NOT NEED IMPLEMENTATION HERE
			pass
		# elif "ico payment" in transaction["Taxable"].lower():
		# 	#ICO Payment - 7 ETH:1903.34 GVT
		# 	#IF ICO PAYMENT
		# 	#DOES NOT NEED IMPLEMENTATION HERE
		# 	pass
		# elif "ico distribution" in transaction["Taxable"].lower():
		# 	#ICO Distribution - 7 ETH:1903.34 GVT
		# 	tranCopy = transaction.copy()
		# 	buyInfo = transaction["Taxable"].split("-")[1].split(":")[1]
		# 	sellAmount,sellCoin = sellInfo.strip().split(" ")
		# 	sellAmount = float(sellAmount)
		# 	buyAmount,buyCoin = buyInfo.strip().split(" ")
		# 	buyAmount = float(buyAmount)
		# 	tranCopy["Buy_Quantity"] = buyAmount
		# 	tranCopy["Sell_Coin"] = sellCoin
		# 	tranCopy["Sell_Quantity"] = sellAmount
		# 	tranCopy["Sell_Coin_Price"] = sellPrice
		# 	print("Bought {} {} on {}".format(tranCopy["Buy_Quantity"],tranCopy["Buy_Coin"],tranCopy["Date"]))
		# 	buyCoins[buyCoin].append(tranCopy)
		elif transaction["Taxable"].lower() in ["borrow","for friend"]:
			borrowed[transaction["Buy_Coin"]].append(transaction)
		elif (transaction["Action"] == "Trade" and transaction["Taxable"]) or (transaction["Action"] == "Deposit" and transaction["Taxable"] in []):
			if logger:
				print("Bought {} {} on {}".format(transaction["Buy_Quantity"],transaction["Buy_Coin"],transaction["Date"]))			
			buyCoins[transaction["Buy_Coin"]].append(transaction)
			portfolio[transaction["Buy_Coin"]] += transaction["Buy_Quantity"]
		elif transaction["Taxable"].lower()=="gifted":
			if logger:
				print("Gifted {} {} on {}".format(transaction["Buy_Quantity"],transaction["Buy_Coin"],transaction["Date"]))			
			buyCoins[transaction["Buy_Coin"]].append(transaction)
			portfolio[transaction["Buy_Coin"]] += transaction["Buy_Quantity"]
		elif transaction["Taxable"].lower() in ["false","return"]:
			pass
		else:
			print("Purchase case is not implemented yet: {}".format(transaction))
			exit(1)

def determineBuyUSDPrice(boughtObject,buyCoinPricingMethod):
	buyPrice = 0.0
	if buyCoinPricingMethod == "Default":
		if boughtObject["Buy_Coin_Price"]:
			buyCoinPricingMethod = "BuyCoin"
		elif boughtObject["Sell_Coin_Price"]:
			buyCoinPricingMethod = "SellCoin"
		else:
			raise ValueError("NO BUYCOIN OR SELLCOIN PRICE")
	if buyCoinPricingMethod == "BuyCoin":
		buyPrice = boughtObject["Buy_Coin_Price"]
	elif buyCoinPricingMethod == "SellCoin":
		dollarsSpent = boughtObject["Sell_Coin_Price"]*boughtObject["Sell_Quantity"]
		coinsBought = boughtObject["Buy_Quantity"]
		buyPrice = dollarsSpent/coinsBought
	if buyPrice:
		return buyPrice
	else:
		raise ValueError("PRICING METHOD FAILED: {}".format(buyCoinPricingMethod))

def determineSellUSDPrice(soldObject,sellCoinPricingMethod):
	sellPrice = 0.0
	if sellCoinPricingMethod == "Default":
		if soldObject["Sell_Coin_Price"]:
			sellCoinPricingMethod = "SellCoin"
		elif soldObject["Buy_Coin_Price"]:
			sellCoinPricingMethod = "BuyCoin"
		else:
			raise ValueError("NO BUYCOIN OR SELLCOIN PRICE")
	if sellCoinPricingMethod == "SellCoin":
		sellPrice = soldObject["Sell_Coin_Price"]
	elif sellCoinPricingMethod == "BuyCoin":
		dollarsReceived = soldObject["Buy_Coin_Price"]*soldObject["Buy_Quantity"]
		coinsSold = soldObject["Sell_Quantity"]
		sellPrice = dollarsReceived/coinsSold
	if sellPrice:
		return sellPrice
	else:
		raise ValueError("PRICING METHOD FAILED: {}".format(sellCoinPricingMethod))

def sell(soldObject,buyCoins,capitalGains,income,assetPriorityMethod="FIFO",sellCoinPricingMethod="Default",buyCoinPricingMethod="Default"):
	global debugging
	capitalGainsForThisTransaction = []
	amountLeft = soldObject["Sell_Quantity"]
	coin = soldObject["Sell_Coin"]
	portfolio[coin] -= amountLeft
	sellPrice = determineSellUSDPrice(soldObject,sellCoinPricingMethod)
	buyCoinsReference = buyCoins
	#print(soldObject)
	if logger:
		print("Selling {} {} for ${:,.2f} per {} $({:,.2f}) on {}".format(amountLeft,coin,sellPrice,coin,amountLeft*sellPrice,soldObject["Date"]))
	if assetPriorityMethod=="FIFO":
		findIndex = lambda x:0
	elif assetPriorityMethod=="LIFO":
		findIndex = lambda x:-1
	elif assetPriorityMethod=="HIFO":
		findIndex = lambda x:x.index(max(x,key=lambda x:x["Buy_Coin_Price"]))
	#print("BEFORE: {}\n".format(buyCoins[coin]))
	while amountLeft > 0.0:
		try:
			purchaseIndex = findIndex(buyCoinsReference[coin])
			purchaseCopy = dict(buyCoinsReference[coin][purchaseIndex])
			buyPrice = determineBuyUSDPrice(buyCoinsReference[coin][purchaseIndex],buyCoinPricingMethod)
			buyDate = buyCoinsReference[coin][purchaseIndex]["Date"]
			if amountLeft >= buyCoinsReference[coin][purchaseIndex]["Buy_Quantity"]:
				poppedBuy = buyCoinsReference[coin].pop(purchaseIndex)
				soldAmount = poppedBuy["Buy_Quantity"]
				amountLeft -= soldAmount
			elif amountLeft < buyCoinsReference[coin][purchaseIndex]["Buy_Quantity"]:
				soldAmount = amountLeft
				buyCoinsReference[coin][purchaseIndex]["Buy_Quantity"]-=amountLeft
				amountLeft = 0
		except IndexError:
			if (amountLeft*sellPrice)<0.01:
				#Check if negligible
				if logger:
					print("Dusted off {:.2} {}".format(amountLeft,coin))
				amountLeft = 0.0
			# elif input("Use Borrowed Coins For Sale of {} {}? (y/n): ".format(amountLeft,coin)).lower()=="y":
			# 	buyCoinsReference = borrowed
			else:
				#Throw Error
				print("ERROR: ${:,.2f} ({:,.10}) worth of {} is unaccounted for.".format(amountLeft*sellPrice,amountLeft,coin))
				exit(1)
		if debugging:
			input()
		dollarsReceived = sellPrice*soldAmount
		dollarsSpent = buyPrice*soldAmount
		realizedGain =  dollarsReceived - dollarsSpent
		if logger:
			print("{:,} {} was sold for ${:,f}\nWhich was initially bought for ${:,f}\nResulting in a realized gain of ${:,f} - ${:,f} = ${:,f}\n".format(soldAmount,coin,dollarsReceived,dollarsSpent,dollarsReceived,dollarsSpent,realizedGain))
		#print("AFTER: {}\n".format(buyCoins[coin]))		
		capitalGainsForThisTransaction.append({"RealizedGain":realizedGain,"Coin":coin,"Buy_Price":buyPrice,"Sell_Price":sellPrice,"Quantity":soldAmount,"Buy_Date":buyDate,"Sell_Date":soldObject["Date"]})
		#print("{}\n\n\n".format(capitalGainsForThisTransaction))
	capitalGains.extend(capitalGainsForThisTransaction)

try:
	user = sys.argv[1]
except IndexError:
	print("ERROR: Must Enter User as Command-Line Argument.")
	exit(1)

dateToTransactions = defaultdict(list)
buyCoins = defaultdict(list)
borrowed = defaultdict(list)
spentOnICO = defaultdict()
gifted = []
secondsInDay = 86400
transactions = []
capitalGains = []
income = []
buyCoinsEpochtimes = set()
portfolio = defaultdict(float)
debugging = False
logger = True
try:
	assetPriorityMethod = sys.argv[2]
	#likeKind = eval(sys.argv[3])
except:
	assetPriorityMethod = "FIFO"
	if logger:
		print("USAGE: python CryptoTaxSummary.py <user> <FIFO or HIFO or LIFO> <(LikeKind) True/False>")
		exit(1)

try:
	transactionLines = open("{}FinalOutputWithChanges2.csv".format(user),"r").read().split("\n")
except FileNotFoundError:
	print("ERROR: {}FinalOutputWithChanges2.csv Not Found.".format(user))
	exit(1)

for transaction in transactionLines[1:]:
	#print(transaction)
	transactionSplit = transaction.split(",")
	keys = "Action,Buy_Quantity,Buy_Coin,Sell_Quantity,Sell_Coin,Exchange,To_Address,From_Address,TxID,Buy_Coin_Price,Sell_Coin_Price,Date,Taxable".split(",")
	transactionObject = {key:val for key,val in zip(keys,transactionSplit)}
	epochtime = int(time.mktime(time.strptime(transactionObject["Date"],"%Y-%m-%d")))
	transactionObject["Epochtime"] = epochtime
	transactionObject["Buy_Quantity"] = float(transactionObject["Buy_Quantity"]) if transactionObject["Buy_Quantity"] else ""
	transactionObject["Sell_Quantity"] = float(transactionObject["Sell_Quantity"]) if transactionObject["Sell_Quantity"] else ""
	transactionObject["Buy_Coin_Price"] = float(transactionObject["Buy_Coin_Price"]) if transactionObject["Buy_Coin_Price"] not in ["","Not Found"] else ""
	transactionObject["Sell_Coin_Price"] = float(transactionObject["Sell_Coin_Price"]) if transactionObject["Sell_Coin_Price"] not in ["","Not Found"] else ""
	transactions.append(transactionObject)
	dateToTransactions[epochtime].append(transactionObject)
for transactionObject in transactions[::-1]:
	taxable = transactionObject["Taxable"].lower()
	if transactionObject["Epochtime"] not in buyCoinsEpochtimes:
		loadTransactionsFromDate(transactionObject["Epochtime"],buyCoins,income)
		buyCoinsEpochtimes.add(transactionObject["Epochtime"])
	# print(taxable)
	# print(transactionObject["Action"])
	if taxable == "false":
		#Not Taxable, SKIP
		pass
	elif (transactionObject["Action"] == "Deposit" ) or (transactionObject["Action"] == "Withdrawal" and taxable in  ["false","fork","airdrop","ico distribution"]):
		pass
	elif transactionObject["Action"] != "Deposit" and transactionObject["Sell_Quantity"]!="" and taxable not in ["false","fork","airdrop","ico distribution"]:
		#IF TRADE OR WITHDRAWAL OF > 0 AND NOT FREE TOKEN
		# if logger:
		# 	print("{} Buys: {}".format(transactionObject["Sell_Coin"],buyCoins[transactionObject["Sell_Coin"]]))
		#print("Sell {} {}".format(transactionObject["Sell_Quantity"],transactionObject["Sell_Coin"]))
		if taxable == "true": 
			if transactionObject["Sell_Coin"]!="USD":
				if not transactionObject["Sell_Coin_Price"]:
					# print(transactionObject)
					sell(transactionObject,buyCoins,capitalGains,income,assetPriorityMethod=assetPriorityMethod,sellCoinPricingMethod="BuyCoin",buyCoinPricingMethod="Default")
				else:
					sell(transactionObject,buyCoins,capitalGains,income,assetPriorityMethod=assetPriorityMethod,sellCoinPricingMethod="Default",buyCoinPricingMethod="Default")
			else:
				pass
		elif "ico payment" in taxable:
			#ICO Payment - 7 ETH:1903.34 GVT
			transactionObjectCopy = transactionObject.copy()
			purchases = taxable.split("-")[1:]
			for purchase in purchases:
				sellInfo,buyInfo = purchase.split(":")
				buyAmount,buyCoin = buyInfo.strip().split(" ")
				sellAmount,sellCoin = sellInfo.strip().split(" ")
				sellPrice = transactionObjectCopy["Sell_Coin_Price"]
				transactionObjectCopy["Buy_Coin"] = buyCoin.upper()
				transactionObjectCopy["Buy_Quantity"] = float(buyAmount)
				transactionObjectCopy["Sell_Coin"] = sellCoin.upper()
				transactionObjectCopy["Sell_Quantity"] = float(sellAmount)
				spentOnICO[buyCoin] = (sellCoin,sellAmount,sellPrice)
			sell(transactionObjectCopy,buyCoins,capitalGains,income,assetPriorityMethod=assetPriorityMethod)	
		elif "ico distribution" in taxable:
			pass
		elif taxable == "for friend":
			# print("txObj: {}".format(transactionObject))
			# print("")
			# print("Borrowed Before: {}".format(borrowed))
			portfolio[transactionObject["Sell_Coin"]] += transactionObject["Sell_Quantity"]
			#Negate the portfolio line in sell function
			sell(transactionObject,borrowed,capitalGains,income,assetPriorityMethod=assetPriorityMethod)
			# print("Borrowed After: {}".format(borrowed))
		elif taxable == "return":
			portfolio[transactionObject["Sell_Coin"]] += transactionObject["Sell_Quantity"]
			#Negate the portfolio line in sell function
			sell(transactionObject,borrowed,capitalGains,income,assetPriorityMethod=assetPriorityMethod)
		elif taxable in ["false"]:
			pass
		else:
			print("Sell case is not implemented yet: {}".format(transactionObject))
			exit(1)
	else:
		print("Cannot Determine Whether Buy or Sell: {}".format(transactionObject))
		exit(1)
	# elif (transactionObject["Action"] == "Trade") or (transactionObject["Action"] == "Deposit" and transactionObject["Taxable"] != "False"):
	# 	print("Buy {} {} on {}".format(transactionObject["Buy_Quantity"],nsactionObject["Buy_Coin"],nsactionObject["Epochtime"]))
	# 	print("{} Buys: {}".format(transactionObject["Buy_Coin"],Coins[transactionObject["Buy_Coin"]]))
totalGains = 0.0
totalIncome = 0.0
for gain in capitalGains:
	totalGains+=gain["RealizedGain"]
	#print(gain)
for payment in income:
	paymentIncome = payment["Buy_Quantity"]*payment["Buy_Coin_Price"]
	print(payment)
	print("TAXABLE: ${:,.2f}".format(paymentIncome))
	print("")
	totalIncome += paymentIncome
print("Realized Gains Using FIFO and Default Pricing: ${:,.2f}".format(totalGains))
print("Income Tax Using FIFO and Default Pricing: ${:,.2f}".format(totalIncome))

# for coin in portfolio:
# 	if portfolio[coin] < -0.001 or portfolio[coin] > 0.001:
# 		print("{}: {:,.4f}".format(coin,portfolio[coin]))

# else:
# 	#Like-Kind
# 	for transaction in transactionLines[1:]:
# 		transactionSplit = transaction.split(",")
# 		keys = "Action,Buy_Quantity,Buy_Coin,Sell_Quantity,Sell_Coin,Exchange,To_Address,From_Address,TxID,Buy_Coin_Price,Sell_Coin_Price,Date,Taxable".split(",")
# 		transactionObject = {key:val for key,val in zip(keys,transactionSplit)}
# 		if transactionObject["Action"] == "Trade":
# 			if transactionObject["Sell_Coin"]=="USD":
# 				buyCoins["Cryptocurrency"].append(transactionObject)