1. Add input files in the following format. Do this for Binance, Bittrex (Trades Only), Coinbase, and Gemini.
	- UserInputFiles/<user>/<exchangeName>/<user><exchange><transactionType>.csv
Examples:
	UserInputFiles/Aris/Binance/ArisBinanceWithdrawals
	UserInputFiles/Aris/Bittrex/ArisBittrexTrades

2. Create API key files in the following format: The first line should be your api key, and the second line should be the api secret key.

<user><abbreviatedExchange>.txt Contents:
<API_KEY>
<API_SECRET_KEY>

The file should be saved in the following directory.

	- UserAPIKeys/<user>/<user><abbreviatedExchange>.txt
	(Abbreviated Exchange Key: Bx - Bittrex, Cr - Cryptopia, K - Kucoin)
		
Examples for Bittrex and Kucoin:
	UserAPIKeys/Aris/ArisBx.txt
	UserAPIKeys/Aris/ArisK.txt

2. Open new Terminal
3. Navigate to this directory
4. Run "python RunAllScripts.py <YOUR NAME>" (Replace <YOUR NAME> with your name...)
