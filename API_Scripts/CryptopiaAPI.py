import base64
import hashlib
import urllib
import hmac
import time
import requests
import json
import re
import sys
from requests.compat import quote_plus

class CryptopiaAPI(object):

	def __init__(self,key,secret):
		self.API_KEY = key
		self.API_SECRET = secret
		
	def callAPIMethod(self,methodName,data={}):
		postParamters = json.dumps(data).encode('utf-8')
		url = "https://www.cryptopia.co.nz/api/{}".format(methodName)
		nonce = str(time.time())
		#md5 hash of post parameters
		#	print("postParamters: {}".format(postParamters))
		md5HashedPostParameters = hashlib.md5(postParamters)
		#	print("md5HashedPostParameters: {}".format(md5HashedPostParameters))
		#Base64 of MD5 Hash of Post Params
		b64_md5_postParams = base64.b64encode(md5HashedPostParameters.digest()).decode('utf-8')
		#	print("b64_md5_postParams: {}".format(b64_md5_postParams))
		#form signature
		signature = self.API_KEY + "POST" + quote_plus(url).lower() + nonce + b64_md5_postParams
		#hmacsig
		hmacsignature = base64.b64encode(hmac.new(base64.b64decode( self.API_SECRET ), signature.encode('utf-8'), hashlib.sha256).digest())
		#The below makes sense
		header_value = "amx " + self.API_KEY + ":" + hmacsignature.decode('utf-8') + ":" + nonce
		headers = { 'Authorization': header_value, 'Content-Type':'application/json; charset=utf-8' }
		r = requests.post(url, data = postParamters, headers = headers )
		if r.status_code != 200 and r.json().get("Data"):
			print("SUCCESS: {}, MSG: {}".format(r.json()["success"],r.json()["msg"]))
			exit(1)
		try:
			return r.json()["Data"]
		except KeyError:
			print("Request.JSON: {}".format(r.json()))
			print("\tCryptopia Script Run Too Often. Wait a few minutes and run again.")
			exit(1)

	def getDepositsAndWithdrawalsFromAPICall(self,data={}):
		return self.callAPIMethod("GetTransactions",data=data)

	def getTradesFromAPICall(self, data={}):
		return self.callAPIMethod("GetTradeHistory",data=data)

	def tradesCSV(self,data={}):
		trades = self.getTradesFromAPICall(data=data)
		# print("Num Trades: {}".format(len(trades)))
		#print(trades)
		lines = []
		action = "Trade"
		exchange = "Cryptopia"
		for trade in trades:
			buy = trade["Type"]=="Buy"
			buyQuantity = trade["Amount"] if buy else trade["Total"]
			buyCoin,sellCoin = trade["Market"].split("/") if buy else trade["Market"].split("/")[::-1]
			sellQuantity = trade["Total"] if buy else trade["Amount"] 
			dateOriginalFormat = trade["TimeStamp"] # 2018-03-28T23:42:33.9419167
			year,month,day,hour,minute,second = re.match('(.*)-(.*)-(.*)T(.*):(.*):(.*)',dateOriginalFormat).groups()
			second = second.split(".")[0]
			date = "{}-{}-{} {}:{}:{}".format(year,month,day,hour,minute,second) #2017-12-17 19:09:14
			lines.append("{},{},{},{},{},{},{},{},{},{}".format(action,buyQuantity,buyCoin,sellQuantity,sellCoin,exchange,"","","",date))
		return "\n".join(lines)

	def depositsAndWithdrawalsCSV(self,data={}):
		transfers = self.getDepositsAndWithdrawalsFromAPICall(data=data)
		#print(transfers)
		# print("NUM TRANSFERS: {}".format(len(transfers)))
		lines = []
		exchange = "Cryptopia"
		for transfer in transfers:
			action = "Deposit" if transfer["Type"]=="Deposit" else "Withdrawal"
			quantity = transfer["Amount"]
			coin = transfer["Currency"]
			dateOriginalFormat = transfer["Timestamp"] # 2018-03-28T23:42:33.9419167
			year,month,day,hour,minute,second = re.match('(.*)-(.*)-(.*)T(.*):(.*):(.*)',dateOriginalFormat).groups()
			second = second.split(".")[0]
			date = "{}-{}-{} {}:{}:{}".format(year,month,day,hour,minute,second) #2017-12-17 19:09:14
			txID = transfer["TxId"]
			if action=="Deposit":
				#print(transfer)
				if transfer["Status"] == "Confirmed":
					toAddress = "Exchange Wallet"
					fromAddress = transfer["Address"]
					lines.append("{},{},{},{},{},{},{},{},{},{}".format(action,quantity,coin,"","",exchange,toAddress,fromAddress,txID,date))
			else:
				if transfer["Status"] == "Complete":
					toAddress = transfer["Address"]
					fromAddress = "Exchange Wallet"
					lines.append("{},{},{},{},{},{},{},{},{},{}".format(action,"","",quantity,coin,exchange,toAddress,fromAddress,txID,date))			
		return "\n".join(lines)

def run(user):
	exchange = "Cryptopia"
	print("Started {} script.".format(exchange))
	try:
		Key,Secret = open("UserAPIKeys/{}/{}Cr.txt".format(user,user),"r").read().split()
		cryptopiaAPI = CryptopiaAPI(Key,Secret)
		outputFilePath = "OutputFiles/{}/{}CryptopiaOutput.csv".format(user,user)
		outputFile = open(outputFilePath,"w")
		trades = cryptopiaAPI.tradesCSV(data={'Count':1000})
		transfers = cryptopiaAPI.depositsAndWithdrawalsCSV()
		outputFile.write(transfers+"\n"+trades)
		print("GENERATED: {}".format(outputFilePath))
		return True
	except FileNotFoundError:
		print("ERROR: {}'s UserAPIKey file for {} was not found.".format(user,exchange))
		return False

if(len(sys.argv) > 2 and sys.argv[2]=="run"):
	run(sys.argv[1])