import requests
import time
import sys

def run(user):
	exchange = "Personal Wallet"
	print("Started Etherplorer script.")
	try:
		addressFilePath = 'UserInputFiles/{}/Addresses/{}EthereumAddresses.txt'.format(user,user)
		ETH_ADDRESSES = open(addressFilePath).read().split("\n")
		outputFilePath = 'OutputFiles/{}/{}ERC20TokenOutput.csv'.format(user,user)
		outputFile = open(outputFilePath,"w")
		lines = []
		apiKeyFilePath = 'UserAPIKeys/{}/ArisEthplorerKey.txt'.format(user,user)
		API_KEY = "freekey"
		for eth_address in ETH_ADDRESSES:
			API_URL = "https://api.neoscan.io/api/main_net/v1/get_address/{}".format(neo_address)
			alltransactions = requests.get(API_URL).json()
			for transaction in alltransactions["operations"]:
				coin = transaction["tokenInfo"]["symbol"]
				# ex = {'timestamp': 1522686378,
				# 'transactionHash': '0x9ce074c4cb14aadfdfb7caaf982b3add8326e3e90ac786cf66bb721a00ac20c9',
				# 'tokenInfo': 
				# 	{'address': '0x80e82dd8707a68d9f26a3035c1bbf2b704549801',
				# 	'name': 'Trendercoin',
				# 	'decimals': '3',
				# 	'symbol': 'TDC',
				# 	'totalSupply': '1000000000000',
				# 	'owner': '0x',
				# 	'txsCount': 761,
				# 	'transfersCount': 9770,
				# 	'lastUpdated': 1522838289,
				# 	'issuancesCount': 0,
				# 	'holdersCount': 9542,
				# 	'price': False},
				# 	'type': 'transfer',
				# 	'value': '2000000',
				# 	'from': '0x62a03c868c959386b2df7f266e79bc711fb92398',
				# 	'to': '0xf9ffce0c69e997e798b76fe3d430be6743dde2ea'}
				txID = transaction["transactionHash"]
				date = time.strftime("%Y-%m-%d %H:%M:%S",time.localtime(int(transaction["timestamp"])))
				quantity = float(transaction["value"])/10**18
				toAddress = transaction["to"]
				fromAddress = transaction["from"]
				#print("FROM: {}\nTO: {}\nAMOUNT: {} ETH\nTIME: {}".format(fromAddress,toAddress,quantity,date))
				#print("{} == {}: {}".format(toAddress,eth_address.lower(),toAddress==eth_address.lower()))
				if toAddress.lower()==eth_address.lower():
					#Deposit
					action = "Deposit"
					lines.append("{},{},{},{},{},{},{},{},{},{}".format(action,quantity,coin,"","",exchange,toAddress,fromAddress,txID,date))
				else:
					#Withdrawal
					action = "Withdrawal"
					lines.append("{},{},{},{},{},{},{},{},{},{}".format(action,"","",quantity,coin,exchange,toAddress,fromAddress,txID,date))
		outputFile.write("\n".join(lines))
		print("GENERATED: {}".format(outputFilePath))
		return True
	except FileNotFoundError as err:
		print("ERROR: {}'s UserInputFile(s) for {} were not found.".format(user,exchange))
		print("ERROR MESSAGE: {}".format(err))
		return False

if(len(sys.argv) > 2 and sys.argv[2]=="run"):
	run(sys.argv[1])