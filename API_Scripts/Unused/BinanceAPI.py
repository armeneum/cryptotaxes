import base64
import hashlib
import urllib
import hmac
import time
import requests
import json
import re
from requests.compat import quote_plus

#INCOMPLETE

class BinanceAPI(object):

	def __init__(self,key,secret):
		self.API_KEY = key
		self.API_SECRET = secret
		self.BASE_URL = "https://api.binance.com"

	def makeAPICall(self,url,reqBody={}):
		nonce = str(int(time.time())*1000)
		urlParams = {"timestamp":nonce}
		totalParams = {}
		signature = hmac.new(self.API_SECRET.encode(),queryString+reqBody,hashlib.sha256).hexdigest()
		r = requests.get(url,params=urlParams)
		return r.json()[""]

	def tradesCSV(self,data={}):
		tradesURL = "{}/api/v3/myTrades".format(baseURL)
		allCoins = ["BTC"]
		trades = self.makeAPICall(tradesURL)
		print(trades)
		exit(1)
		lines = []
		# action = "Trade"
		# exchange = "Binance"
		# for trade in trades:
		# 	buy = "BUY" in trade["OrderType"]
		# 	buyQuantity,sellQuantity = [trade["Quantity"],trade["Price"]] if buy else [trade["Price"],trade["Quantity"]]
		# 	buyCoin,sellCoin = trade["Exchange"].split("-") if buy else trade["Exchange"].split("-")[::-1]
		# 	dateOriginalFormat = trade["TimeStamp"] # 2018-03-28T23:42:33.9419167
		# 	year,month,day,hour,minute,second = re.match('(.*)-(.*)-(.*)T(.*):(.*):(.*)',dateOriginalFormat).groups()
		# 	second = second.split(".")[0]
		# 	date = "{}-{}-{} {}:{}:{}".format(year,month,day,hour,minute,second) #2017-12-17 19:09:14
		# 	lines.append("{},{},{},{},{},{},{},{},{},{},{}".format(action,buyQuantity,buyCoin,sellQuantity,sellCoin,"","",exchange,"","",date))
		return "\n".join(lines)

	def depositsAndWithdrawalsCSV(self,data={}):
		deposits = self.makeAPICall("getdeposithistory")
		withdrawals = self.makeAPICall("getwithdrawalhistory")
		#print(transfers)
		# print("NUM TRANSFERS: {}".format(len(transfers)))
		lines = []
		exchange = "Bittrex"
		transferDict = {"Deposit":deposits,"Withdrawal":withdrawals}
		for action in transferDict:
			dateKey = "LastUpdated" if action=="Deposit" else "Opened"
			transfers = transferDict[action]
			for transfer in transfers:
				quantity = transfer["Amount"]
				coin = transfer["Currency"]
				dateOriginalFormat = transfer[dateKey] # 2018-03-28T23:42:33.9419167
				year,month,day,hour,minute,second = re.match('(.*)-(.*)-(.*)T(.*):(.*):(.*)',dateOriginalFormat).groups()
				second = second.split(".")[0]
				date = "{}-{}-{} {}:{}:{}".format(year,month,day,hour,minute,second) #2017-12-17 19:09:14
				if action=="Deposit":
					lines.append("{},{},{},{},{},{},{},{},{},{},{}".format(action,quantity,coin,"","","","",exchange,"","",date))
				else:
					lines.append("{},{},{},{},{},{},{},{},{},{},{}".format(action,"","",quantity,coin,"","",exchange,"","",date))			
		return "\n".join(lines)


Key,Secret = open("Bn.txt","r").read().split()
binanceAPI = BinanceAPI(Key,Secret)
outputFile = open("Binance.csv","w")
trades = binanceAPI.tradesCSV()
#transfers = binanceAPI.depositsAndWithdrawalsCSV()
#outputFile.write(transfers+"\n"+trades)