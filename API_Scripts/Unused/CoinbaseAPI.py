from coinbase.wallet.client import Client

def transfersToList(deposits,withdrawals):
	print("Deposits: {}".format(deposits))
	print("Withdrawals: {}".format(withdrawals))

def transactionsToList(transactions):
	pass

user = "Aris"
exchange = "Coinbase"
outputFile = open("../OutputFiles/{}/{}{}Output.csv".format(user,user,exchange),"w")
API_KEY,API_SECRET = open("../UserAPIKeys/{}/Cb.txt".format(user),"r").read().split()
client = Client(API_KEY, API_SECRET)
accounts = client.get_accounts()["data"]
lines = []
for account in accounts:
	coin = account["currency"]
	accountID = account["id"]
	print("{}: {}".format(coin,accountID))
	deposits = None
	withdrawals = None
	try:
		getDeposits = client.get_deposits(accountID)
		deposits = getDeposits["data"]
	except coinbase.wallet.error.InternalServerError:
		print("DEPOSIT ERROR")
	try:
		getWithdrawals = client.get_withdrawals(accountID)
		withdrawals = getWithdrawals["data"]
	except:
		print("WITHDRAWAL ERROR")
	transfers = transfersToList(deposits,withdrawals)
	# transactions = transactionsToList(client.get_transactions(accountID)["data"])
	# lines.extend(transfers+transactions)
#outputFile.write("\n".join(lines))