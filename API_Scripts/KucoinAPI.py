import base64
import hashlib
import hmac
import time
import requests
import sys

class KucoinAPI(object):
	API_URL = "https://api.kucoin.com"
	API_VERSION = "v1"

	def __init__(self, api_key, api_secret, user):
		self.headers = {}
		self.headers["KC-API-KEY"] = api_key
		self.API_SECRET = api_secret
		self.user = user

	def setSignature(self, nonce, endpoint, data={}):
		#order parameters like amount=10&price=1.1&type=BUY
		queryString = KucoinAPI.dataToQueryString(data)
		#splice string for signing
		sig_str = ("{}/{}/{}".format(endpoint, nonce, queryString)).encode('utf-8')
		#print("SIG_STRING: {}".format(sig_str))
		#Made base64 ecoding of the completed string
		m = hmac.new(self.API_SECRET.encode('utf-8'), base64.b64encode(sig_str), hashlib.sha256)
		self.headers["KC-API-SIGNATURE"] = m.hexdigest()

	def getDepositsAndWithdrawalsByCoin(self,coin):
		nonce = int(time.time()*1000)
		self.headers["KC-API-NONCE"] = str(nonce)
		endpoint = "/{}/account/{}/wallet/records".format(self.API_VERSION,coin)
		url = self.API_URL+endpoint
		self.setSignature(nonce,endpoint)
		#print("-=Making Request=-")
		req = {"headers":self.headers, "route":url}
		#print(req)
		r = requests.get(url,headers=self.headers)
		return r.json()

	def getCoinBalance(self,coin):
		nonce = int(time.time()*1000)
		self.headers["KC-API-NONCE"] = str(nonce)
		endpoint = "/{}/account/{}/balance".format(self.API_VERSION,coin)
		url = self.API_URL+endpoint
		self.setSignature(nonce,endpoint)
		#print("-=Making Request=-")
		req = {"headers":self.headers, "route":url}
		#print(req)
		r = requests.get(url,headers=self.headers)
		return r.json()

	def getBalances(self,limit=None,page=None):
		# params = {}
		# if limit: params["limit"] = limit
		# if page: params["page"] = page
		nonce = int(time.time()*1000)
		self.headers["KC-API-NONCE"] = str(nonce)
		#queryString = KucoinAPI.dataToQueryString(params)
		endpoint = "/{}/account/balance".format(self.API_VERSION)
		url = self.API_URL+endpoint
		self.setSignature(nonce,endpoint)
		r = requests.get(url, headers=self.headers)#, params={"limit":limit,"page":page})
		req = {"headers":self.headers, "route":r.url}
		# print("-=Making Request=-")
		# print(req)
		return r.json()

	#UNTESTED FOR ACCOUNTS WITH > 100 TRADES
	def getTradeHistory(self,symbol=None,type=None,limit=None,page=None,since=None,before=None):
		params = {}
		if limit: params["limit"] = limit
		if page: params["page"] = page
		nonce = int(time.time()*1000)
		self.headers["KC-API-NONCE"] = str(nonce)
		#queryString = KucoinAPI.dataToQueryString(params)
		endpoint = "/{}/order/dealt".format(self.API_VERSION)
		url = self.API_URL+endpoint
		self.setSignature(nonce,endpoint,params)
		r = requests.get(url, headers=self.headers, params=params)
		# req = {"headers":self.headers, "route":r.url}
		# print("-=Making Request=-")
		# print(req)
		return r.json()		

	#DEPOSITS AND WITHDRAWALS
	def depositsAndWithdrawalsCSV(self):
		coins = []
		if self.user == "Aris":
			coins = ["KCS","BTC","ARY","BTCP","GOD","BCD","ETF","ABTC","BNTY","ETH", "POLL", "PBL"]
		elif self.user == "Haig":
			coins = ["ARY","TKY","ETH"]
		else:
			response = self.getBalances()
			coins = [data["coinType"] for data in response["data"]]
		transactionPages = []
		for coin in coins:
			tran = self.getDepositsAndWithdrawalsByCoin(coin)["data"]["datas"]
			if tran:
				transactionPages.append(tran)
				print("\tKucoin: Added {} transaction page.".format(coin))
		lines = []
		for page in transactionPages:
			for transaction in page:
				#print(transaction["status"])
				if transaction["status"]=="SUCCESS":
					action = "Deposit" if transaction["type"]=="DEPOSIT" else "Withdrawal"
					quantity = transaction["amount"]
					coin = transaction["coinType"]
					exchange = "Kucoin"
					date = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(transaction["createdAt"]/1000))
					txID = transaction["outerWalletTxid"]
					#print("Before: {}".format(txID))
					if txID and "@" in txID:
						txID = txID.split("@")[0]
					#print("After: {}".format(txID))
					if action=="Deposit":
						toAddress = "Exchange Wallet"
						fromAddress = transaction["address"]
						lines.append("{},{},{},{},{},{},{},{},{},{}".format(action,quantity,coin,"","",exchange,toAddress,fromAddress,txID,date))
					else:
						toAddress = transaction["address"]
						fromAddress = "Exchange Wallet"
						lines.append("{},{},{},{},{},{},{},{},{},{}".format(action,"","",quantity,coin,exchange,toAddress,fromAddress,txID,date))			
		return "\n".join(lines)

	def tradesCSV(self):
		tradesFromAPICall = self.getTradeHistory(limit=100,page=1)
		#print("SUCCESS: {}, MSG: {}".format(tradesFromAPICall["success"],tradesFromAPICall["msg"]))
		lines = []
		if tradesFromAPICall["success"]:
			action = "Trade"
			exchange = "Kucoin"
			for trade in tradesFromAPICall["data"]["datas"]:
				buy = trade["direction"]=="BUY"
				buyQuantity = trade["amount"] if buy else trade["dealValue"]
				buyCoin = trade["coinType"] if buy else trade["coinTypePair"]
				sellQuantity = trade["dealValue"] if buy else trade["amount"] 
				sellCoin = trade["coinTypePair"] if buy else trade["coinType"]
				date = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(trade["createdAt"]/1000))
				lines.append("{},{},{},{},{},{},{},{},{},{}".format(action,buyQuantity,buyCoin,sellQuantity,sellCoin,exchange,"","","",date))
			#print("Total Trades Received: {}".format(len(tradesFromAPICall["data"]["datas"])))
		return "\n".join(lines)

	def dataToQueryString(data):
		strs = []
		for key in sorted(data):
			strs.append("{}={}".format(key, data[key]))
		return '&'.join(strs)

def run(user):
	exchange = "Kucoin"
	print("Started {} script.".format(exchange))
	try:
		Key,Secret = open("UserAPIKeys/{}/{}K.txt".format(user,user),"r").read().split()
		kucoinAPI = KucoinAPI(Key,Secret,user)
		outputFileName = "OutputFiles/{}/{}KucoinOutput.csv".format(user,user)
		outputFile = open(outputFileName,"w")
		depositsAndWithdrawals = kucoinAPI.depositsAndWithdrawalsCSV()
		trades = kucoinAPI.tradesCSV()
		outputFile.write(depositsAndWithdrawals+"\n"+trades)
		print("GENERATED: {}".format(outputFileName))
		return True
	except FileNotFoundError:
		print("ERROR: {}'s UserAPIKey file for {} was not found.".format(user,exchange))
		return False

if(len(sys.argv) > 2 and sys.argv[2]=="run"):
	run(sys.argv[1])