import requests
import time
import sys
import json

def run(user):
	exchange = "Personal Wallet"
	print("Started Etherscan script.")
	try:
		addressFilePath = 'UserInputFiles/{}/Addresses/{}EthereumAddresses.txt'.format(user,user)
		ETH_ADDRESSES = open(addressFilePath).read().split("\n")
		outputFilePath = 'OutputFiles/{}/{}EthereumOutput.csv'.format(user,user)
		outputFile = open(outputFilePath,"w")
		lines = []
		numEthAddresses = len(ETH_ADDRESSES)
		ethAddressCount = 0
		for eth_address in ETH_ADDRESSES:
			print("\tAdding Transactions For {}...".format(eth_address))
			ethAddressCount += 1
			print("\t\tMaking API Calls...")
			normal_Transactions_API_URL = "http://api.etherscan.io/api?module=account&action=txlist&address={}&startblock=0&endblock=99999999&sort=asc".format(eth_address)
			normalTransactions = requests.get(normal_Transactions_API_URL).json()
			tokenTransferCount = 1
			internal_Transactions_API_URL = "http://api.etherscan.io/api?module=account&action=txlistinternal&address={}".format(eth_address)			
			internalTransactions = requests.get(internal_Transactions_API_URL).json()
			token_FromTransactions_API_URL = "https://api.etherscan.io/api?module=logs&action=getLogs&fromBlock=0&toBlock=latest&topic0=0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef&topic1=0x000000000000000000000000{}".format(eth_address[2:])
			tokenFromTransactions = requests.get(token_FromTransactions_API_URL).json()
			token_ToTransactions_API_URL = "https://api.etherscan.io/api?module=logs&action=getLogs&fromBlock=0&toBlock=latest&topic0=0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef&topic2=0x000000000000000000000000{}".format(eth_address[2:])
			tokenToTransactions = requests.get(token_ToTransactions_API_URL).json()
			numTransactions = len(normalTransactions["result"]+internalTransactions["result"])
			normalTransactionCount = 1
			print("\t\tAdding Normal Transactions...")
			for transaction in normalTransactions["result"]+internalTransactions["result"]:
				if transaction["isError"] == "0" and transaction["value"] != "0":
					#Transaction did not fail and is not a token transfer.
					normalTransactionCount+=1
					txID = transaction["hash"]
					coin = "ETH"
					quantity = float(transaction["value"])/10**18
					date = time.strftime("%Y-%m-%d %H:%M:%S",time.localtime(int(transaction["timeStamp"])))
					toAddress = transaction["to"]
					fromAddress = transaction["from"]
					if toAddress.lower()==eth_address.lower():
						#Deposit
						action = "Deposit"
						lines.append("{},{},{},{},{},{},{},{},{},{}".format(action,quantity,coin,"","",exchange,toAddress,fromAddress,txID,date))
					else:
						#Withdrawal
						action = "Withdrawal"
						lines.append("{},{},{},{},{},{},{},{},{},{}".format(action,"","",quantity,coin,exchange,toAddress,fromAddress,txID,date))
				else:
					#Transaction Failed or was a token transfer, so ignore it.
					pass
			totalTokenDeposits = len(tokenToTransactions["result"])
			for tokenDeposits in tokenToTransactions["result"]:
				print("\t\tAdding Token Deposit {}/{}".format(tokenTransferCount,totalTokenDeposits),end="\r")
				tokenTransferCount+=1
				action = "Deposit"
				txID = tokenDeposits["transactionHash"]
				txInfoURL = "https://api.ethplorer.io/getTxInfo/{}?apiKey=freekey".format(txID)
				r = requests.get(txInfoURL).json()
				tokenDeposit = list(filter(lambda x:x["to"].lower()==eth_address.lower(), r["operations"]))[0]
				coin = tokenDeposit["tokenInfo"]["symbol"]
				quantity = float(tokenDeposit["value"])/10**18
				date = time.strftime("%Y-%m-%d %H:%M:%S",time.localtime(int(tokenDeposit["timestamp"])))				
				toAddress = eth_address
				fromAddress = tokenDeposit["from"]
				lines.append("{},{},{},{},{},{},{},{},{},{}".format(action,quantity,coin,"","",exchange,toAddress,fromAddress,txID,date))
			print("\t\tAdded Token Deposits {}/{}".format(tokenTransferCount-1,totalTokenDeposits))
			tokenTransferCount=1
			totalTokenWithdrawals = len(tokenFromTransactions["result"])
			for tokenWithdrawals in tokenFromTransactions["result"]:
				print("\t\tAdding Token Withdrawal {}/{}".format(tokenTransferCount,totalTokenWithdrawals),end="\r")
				tokenTransferCount+=1
				action = "Withdrawal"				
				txID = tokenWithdrawals["transactionHash"]
				txInfoURL = "https://api.ethplorer.io/getTxInfo/{}?apiKey=freekey".format(txID)
				r = requests.get(txInfoURL).json()				
				tokenWithdrawal = list(filter(lambda x:x["from"].lower()==eth_address.lower(), r["operations"]))[0]
				coin = tokenWithdrawal["tokenInfo"]["symbol"]
				quantity =float(tokenWithdrawal["value"])/10**18
				date = time.strftime("%Y-%m-%d %H:%M:%S",time.localtime(int(tokenWithdrawal["timestamp"])))								
				toAddress = tokenWithdrawal["to"]
				fromAddress = eth_address
				lines.append("{},{},{},{},{},{},{},{},{},{}".format(action,"","",quantity,coin,exchange,toAddress,fromAddress,txID,date))
			print("\t\tAdded Token Withdrawals {}/{}".format(tokenTransferCount-1,totalTokenWithdrawals))
			print("\tAdded Transactions For {}".format(eth_address))
		#print("Transfers: {}".format(normalTransactionCount))
		#print("TokenTransfers: {}".format(tokenTransferCount))
		outputFile.write("\n".join(lines))
		print("GENERATED: {}".format(outputFilePath))
		return True
	except FileNotFoundError as err:
		print("ERROR: {}'s UserInputFile(s) for {} were not found.".format(user,exchange))
		print("ERROR MESSAGE: {}".format(err))
		return False

if(len(sys.argv) > 2 and sys.argv[2]=="run"):
	run(sys.argv[1])