import base64
import hashlib
import urllib
import hmac
import time
import requests
import json
import re
import sys
from requests.compat import quote_plus

class BittrexAPI(object):

	def __init__(self,key,secret):
		self.API_KEY = key
		self.API_SECRET = secret

	def makeAPICall(self,method,data={}):
		nonce = str(int(time.time()))
		paramString = "?apikey={}&nonce={}".format(self.API_KEY,nonce)
		url = "https://bittrex.com/api/v1.1/account/{}".format(method)+paramString
		signature = hmac.new(self.API_SECRET.encode(),url.encode(),hashlib.sha512).hexdigest()
		headers = {'apisign':signature}
		r = requests.get(url,headers=headers)
		return r.json()["result"]

	def depositsAndWithdrawalsCSV(self,exchange,data={}):
		deposits = self.makeAPICall("getdeposithistory")
		withdrawals = self.makeAPICall("getwithdrawalhistory")
		#print(transfers)
		# print("NUM TRANSFERS: {}".format(len(transfers)))
		lines = []
		transferDict = {"Deposit":deposits,"Withdrawal":withdrawals}
		for action in transferDict:
			dateKey = "LastUpdated" if action=="Deposit" else "Opened"
			transfers = transferDict[action]
			for transfer in transfers:
				quantity = transfer["Amount"]
				coin = transfer["Currency"]
				dateOriginalFormat = transfer[dateKey] # 2018-03-28T23:42:33.9419167
				year,month,day,hour,minute,second = re.match('(.*)-(.*)-(.*)T(.*):(.*):(.*)',dateOriginalFormat).groups()
				second = second.split(".")[0]
				date = "{}-{}-{} {}:{}:{}".format(year,month,day,hour,minute,second) #2017-12-17 19:09:14
				txID = transfer["TxId"]
				if action=="Deposit":
					toAddress = "Exchange Wallet"
					fromAddress = transfer["CryptoAddress"]
					if len(fromAddress) < 20:
						fromAddress = "Unavailable"
					lines.append("{},{},{},{},{},{},{},{},{},{}".format(action,quantity,coin,"","",exchange,toAddress,fromAddress,txID,date))
				else:
					toAddress = transfer["Address"]
					if len(toAddress) < 20:
						toAddress = "Unavailable"
					fromAddress = "Exchange Wallet"
					lines.append("{},{},{},{},{},{},{},{},{},{}".format(action,"","",quantity,coin,exchange,toAddress,fromAddress,txID,date))			
		return "\n".join(lines)

def run(user):
	exchange = "Bittrex"
	print("Started {} script.".format(exchange))
	try:
		Key,Secret = open("UserAPIKeys/{}/{}Bx.txt".format(user,user),"r").read().split()
		bittrexAPI = BittrexAPI(Key,Secret)
		outputFilePath = "OutputFiles/{}/{}BittrexTransferOutput.csv".format(user,user)
		outputFile = open(outputFilePath,"w")
		transfers = bittrexAPI.depositsAndWithdrawalsCSV(exchange)
		outputFile.write(transfers)
		print("GENERATED: {}".format(outputFilePath))
		return True
	except FileNotFoundError:
		print("ERROR: {}'s UserAPIKey file for {} was not found.".format(user,exchange))
		return False

if(len(sys.argv) > 2 and sys.argv[2]=="run"):
	run(sys.argv[1])